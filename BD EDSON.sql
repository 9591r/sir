CREATE DATABASE  IF NOT EXISTS `aseguramiento` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `aseguramiento`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: aseguramiento
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `calidad_materia_prima`
--

DROP TABLE IF EXISTS `calidad_materia_prima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calidad_materia_prima` (
  `ID_CALIDAD_SELECCION` int(11) NOT NULL AUTO_INCREMENT,
  `ID_MATERIA_PRIMA` int(11) NOT NULL,
  `ID_PROVEEDOR` int(11) NOT NULL,
  `ID_LIMPIEZA` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_CALIDAD_SELECCION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calidad_materia_prima`
--

LOCK TABLES `calidad_materia_prima` WRITE;
/*!40000 ALTER TABLE `calidad_materia_prima` DISABLE KEYS */;
/*!40000 ALTER TABLE `calidad_materia_prima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cantidad_venta_adicional`
--

DROP TABLE IF EXISTS `cantidad_venta_adicional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cantidad_venta_adicional` (
  `ID_CANTIDAD_ADIC` int(11) NOT NULL AUTO_INCREMENT,
  `ID_MATERIA_PRIMA` int(11) NOT NULL,
  `CANTIDAD_MATERIA` int(11) NOT NULL,
  `ID_PRODUCTO_ADICIONAL` int(11) NOT NULL,
  `CANTIDAD_PRODUCTO_ADICIONAL` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  `MONTO_VENTA` int(11) NOT NULL,
  `FECHA_REGISTRO` int(11) NOT NULL,
  PRIMARY KEY (`ID_CANTIDAD_ADIC`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cantidad_venta_adicional`
--

LOCK TABLES `cantidad_venta_adicional` WRITE;
/*!40000 ALTER TABLE `cantidad_venta_adicional` DISABLE KEYS */;
/*!40000 ALTER TABLE `cantidad_venta_adicional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cantidad_ventas`
--

DROP TABLE IF EXISTS `cantidad_ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cantidad_ventas` (
  `ID_CANTIDAD_VENTAS` int(11) NOT NULL AUTO_INCREMENT,
  `ID_MATERIA_PRIMA` int(11) NOT NULL,
  `CANTIDAD_MATERIA` int(11) NOT NULL,
  `ID_PRODUCTO_FABRICADO` int(11) NOT NULL,
  `CANTIDAD_PRODUCTO` int(11) NOT NULL,
  `ID_CLIENTE` int(11) NOT NULL,
  `ID_TIPO_SOLICITUD` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  `MONTO_VENTA` float NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_CANTIDAD_VENTAS`),
  KEY `FK_PRODUCTO` (`ID_PRODUCTO_FABRICADO`),
  KEY `FK_MATERIA_PRIMA` (`ID_MATERIA_PRIMA`),
  KEY `FK_EMPLEADO` (`ID_EMPLEADO`),
  KEY `FK_CLIENTE` (`ID_CLIENTE`),
  KEY `FK_TIPO_SOLICITUD` (`ID_TIPO_SOLICITUD`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cantidad_ventas`
--

LOCK TABLES `cantidad_ventas` WRITE;
/*!40000 ALTER TABLE `cantidad_ventas` DISABLE KEYS */;
INSERT INTO `cantidad_ventas` VALUES (4,1,55,4,3,1,1,3,305.24,'2018-11-08 18:36:08'),(2,2,30,3,20,2,2,2,485.3,'2018-11-08 03:01:41'),(5,1,5345,5,435,2,1,3,436.65,'2018-11-08 18:36:56');
/*!40000 ALTER TABLE `cantidad_ventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `ID_CLIENTE` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_CLIENTE` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_CLIENTE`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Jose'),(2,'Prueba');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contratacion_objetivos`
--

DROP TABLE IF EXISTS `contratacion_objetivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contratacion_objetivos` (
  `CONTRATACION_OBJETIVOS` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_EMPLEADO` varchar(45) DEFAULT NULL,
  `ID_EMPLEADO` varchar(45) DEFAULT NULL,
  `FECHA_INGRESO` date DEFAULT NULL,
  `FECHA_CUMPLIMIENTO` date DEFAULT NULL,
  PRIMARY KEY (`CONTRATACION_OBJETIVOS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contratacion_objetivos`
--

LOCK TABLES `contratacion_objetivos` WRITE;
/*!40000 ALTER TABLE `contratacion_objetivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contratacion_objetivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eleccion_proveedores`
--

DROP TABLE IF EXISTS `eleccion_proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eleccion_proveedores` (
  `ID_ELECCION_PROV` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PROVEEDOR` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  `FECHA_SOLICITUD` date NOT NULL,
  `FECHA_CONTESTACION` date NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_ELECCION_PROV`),
  KEY `FK_EMPLEADO` (`ID_EMPLEADO`),
  KEY `FK_PROVEEDOR` (`ID_PROVEEDOR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eleccion_proveedores`
--

LOCK TABLES `eleccion_proveedores` WRITE;
/*!40000 ALTER TABLE `eleccion_proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `eleccion_proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `EMPLEADO` int(11) NOT NULL AUTO_INCREMENT,
  `PRIMER_NOMBRE` varchar(45) DEFAULT NULL,
  `SEGUNDO_NOMBRE` varchar(45) DEFAULT NULL,
  `PRIMER_APELLIDO` varchar(45) DEFAULT NULL,
  `SEGUNDO_APELLIDO` varchar(45) DEFAULT NULL,
  `DPI` int(20) DEFAULT NULL,
  `DIRECCION` varchar(45) DEFAULT NULL,
  `TELEFONO` int(8) DEFAULT NULL,
  `PUESTO` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`EMPLEADO`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'PRUEBA','PRUEBA','PRUEBA','PRUEBA',123,'65464',12345678,'1'),(2,'RUBEN','PRUEBA','PRUEBA','PRUEBA',123,'65464',12345678,'1'),(3,'AXEL','PRUEBA','PRUEBA','PRUEBA',123,'68468',12345678,'1'),(4,'Edson','Ariel','Aju','Garcia',64154,'158 ffg 5782 09',42584925,'GG');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `limpieza`
--

DROP TABLE IF EXISTS `limpieza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `limpieza` (
  `ID_LIMPIEZA` int(11) NOT NULL,
  `TIPO LIMPIEZA` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `limpieza`
--

LOCK TABLES `limpieza` WRITE;
/*!40000 ALTER TABLE `limpieza` DISABLE KEYS */;
/*!40000 ALTER TABLE `limpieza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materias_primas`
--

DROP TABLE IF EXISTS `materias_primas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materias_primas` (
  `ID_MATERIA_PRIMA` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_MATERIA_PRIMA` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_MATERIA_PRIMA`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materias_primas`
--

LOCK TABLES `materias_primas` WRITE;
/*!40000 ALTER TABLE `materias_primas` DISABLE KEYS */;
INSERT INTO `materias_primas` VALUES (1,'ACERO'),(2,'COBRE'),(3,'HIERRO');
/*!40000 ALTER TABLE `materias_primas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_interno`
--

DROP TABLE IF EXISTS `pedido_interno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_interno` (
  `ID_PEDIDO_INT` int(11) NOT NULL AUTO_INCREMENT,
  `ID_MATERIA_PRIMA` int(11) NOT NULL,
  `CANTIDAD` int(11) NOT NULL,
  `TIEMPO_SOLICITUD` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  `TIEMPO_MAXIMO` int(11) NOT NULL,
  `TIEMPO_MINIMO` int(11) NOT NULL,
  `VALORACION` varchar(50) NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_PEDIDO_INT`),
  KEY `FK_EMPLEADO` (`ID_EMPLEADO`),
  KEY `FK_MATERIA_PRIMA` (`ID_MATERIA_PRIMA`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_interno`
--

LOCK TABLES `pedido_interno` WRITE;
/*!40000 ALTER TABLE `pedido_interno` DISABLE KEYS */;
INSERT INTO `pedido_interno` VALUES (6,1,50,60,4,50,20,'DEBE MEJORAR','2018-11-08 22:08:22'),(7,2,70,15,4,50,20,'EXCELENTE','2018-11-08 22:09:27');
/*!40000 ALTER TABLE `pedido_interno` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER TIEMPO_MAX_MIN BEFORE INSERT on pedido_interno
FOR EACH ROW
BEGIN
	SET NEW.`TIEMPO_MAXIMO` = 50;
    SET NEW.`TIEMPO_MINIMO` = 20;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `PRODUCTO` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_PRODUCTO` varchar(45) NOT NULL,
  `COLOR_PRODUCTO` varchar(10) NOT NULL,
  PRIMARY KEY (`PRODUCTO`),
  UNIQUE KEY `NOMBRE_PRODUCTO_UNIQUE` (`NOMBRE_PRODUCTO`),
  UNIQUE KEY `COLOR_PRODUCTO_UNIQUE` (`COLOR_PRODUCTO`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,'Tapaderas de Hierro Fundido','#000000'),(2,'Rejillas de Hierro Fundido','#00FF00'),(3,'Rejillas de Aluminio y hierro fundido','#FF0000'),(4,'Rejillas de bronce','#0000FF'),(5,'Reposaderas de aluminio y bronce','#999999'),(6,'PRUEBA','#ffdf2b'),(7,'qwerty','#d242d2');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto_adicional`
--

DROP TABLE IF EXISTS `producto_adicional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto_adicional` (
  `ID_PRODUCTO_ADICIONAL` int(11) NOT NULL,
  `NOMBRE_PRODUCTO_ADICIONAL` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto_adicional`
--

LOCK TABLES `producto_adicional` WRITE;
/*!40000 ALTER TABLE `producto_adicional` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto_adicional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto_fabricado`
--

DROP TABLE IF EXISTS `producto_fabricado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto_fabricado` (
  `PRODUCTO_FABRICADO` int(11) NOT NULL AUTO_INCREMENT,
  `CANTIDAD_FABRICADO` int(11) NOT NULL,
  `PRODUCTO` varchar(45) NOT NULL,
  `MES_FECHA` date NOT NULL,
  `COMENTARIO` tinytext,
  PRIMARY KEY (`PRODUCTO_FABRICADO`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto_fabricado`
--

LOCK TABLES `producto_fabricado` WRITE;
/*!40000 ALTER TABLE `producto_fabricado` DISABLE KEYS */;
INSERT INTO `producto_fabricado` VALUES (1,50,'1','2018-11-01','AQUI'),(2,200,'1','2018-11-01',''),(3,50,'2','2018-11-01',''),(4,300,'1','2018-10-01',''),(5,60,'2','2018-10-01',''),(6,75,'1','2018-09-01',''),(7,75,'2','2018-09-01',''),(8,50,'6','2018-10-01','holas');
/*!40000 ALTER TABLE `producto_fabricado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `ID_PROVEEDOR` int(11) NOT NULL,
  `PROVEEDOR` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `puesto`
--

DROP TABLE IF EXISTS `puesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `puesto` (
  `PUESTO` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_PUESTO` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PUESTO`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `puesto`
--

LOCK TABLES `puesto` WRITE;
/*!40000 ALTER TABLE `puesto` DISABLE KEYS */;
/*!40000 ALTER TABLE `puesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitud_compra`
--

DROP TABLE IF EXISTS `solicitud_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitud_compra` (
  `ID_SOLICITUD` int(11) NOT NULL AUTO_INCREMENT,
  `ID_MATERIA_PRIMA` int(11) NOT NULL,
  `CANTIDAD` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  `ID_PROVEEDOR` int(11) NOT NULL,
  `FECHA_ENTREGA` date NOT NULL,
  `FECHA_MAXIMA` datetime NOT NULL,
  `FECHA_MINIMA` datetime NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_SOLICITUD`),
  KEY `FK_EMPLEADO` (`ID_EMPLEADO`),
  KEY `FK_MATERIA_PRIMA` (`ID_MATERIA_PRIMA`),
  KEY `FK_PROVEEDOR` (`ID_PROVEEDOR`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitud_compra`
--

LOCK TABLES `solicitud_compra` WRITE;
/*!40000 ALTER TABLE `solicitud_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitud_compra` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER FECHA_MAX_MIN BEFORE INSERT on solicitud_compra 
FOR EACH ROW 
BEGIN 
SET NEW.`FECHA_MAXIMA` = DATE_ADD(NOW(), INTERVAL 7 DAY); 
SET NEW.`FECHA_MINIMA` = DATE_ADD(NOW(), INTERVAL 2 DAY);  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tiempo_espera_pedido`
--

DROP TABLE IF EXISTS `tiempo_espera_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiempo_espera_pedido` (
  `ID_TIEMPO_ESPERA_PEDIDO` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PROVEEDOR` int(11) NOT NULL,
  `ID_EMPLEADO` int(11) NOT NULL,
  `TIEMPO_REAL` int(11) NOT NULL,
  `TIEMPO_MAXIMO` int(11) NOT NULL,
  `TIEMPO_MINIMO` int(11) NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_TIEMPO_ESPERA_PEDIDO`),
  KEY `FK_EMPLEADO` (`ID_EMPLEADO`),
  KEY `FK_PROVEEDOR` (`ID_PROVEEDOR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiempo_espera_pedido`
--

LOCK TABLES `tiempo_espera_pedido` WRITE;
/*!40000 ALTER TABLE `tiempo_espera_pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiempo_espera_pedido` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER FACTOR_TIEMPO_MAX_MIN BEFORE INSERT on tiempo_espera_pedido 
FOR EACH ROW 
BEGIN 
SET NEW.`TIEMPO_MAXIMO` = 50; 
SET NEW.`TIEMPO_MINIMO` = 20;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tipo_solicitudo`
--

DROP TABLE IF EXISTS `tipo_solicitudo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_solicitudo` (
  `ID_TIPO_SOLICITUD` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO_SOLICITUD` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_TIPO_SOLICITUD`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_solicitudo`
--

LOCK TABLES `tipo_solicitudo` WRITE;
/*!40000 ALTER TABLE `tipo_solicitudo` DISABLE KEYS */;
INSERT INTO `tipo_solicitudo` VALUES (1,'Telefono'),(2,'Red Social');
/*!40000 ALTER TABLE `tipo_solicitudo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `IDUsuario` int(11) unsigned NOT NULL,
  `Usuario` varchar(25) NOT NULL,
  `Password` varchar(25) NOT NULL,
  `IDEmp` int(11) NOT NULL,
  `privilegio` int(11) NOT NULL,
  KEY `FK_EMPLEADO` (`IDEmp`),
  CONSTRAINT `FK_EMPLEADO` FOREIGN KEY (`IDEmp`) REFERENCES `empleado` (`EMPLEADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Consulta','12345',1,1),(2,'Auditor','12345',2,2),(3,'Supervisor','12345',3,3);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-08 22:22:34
