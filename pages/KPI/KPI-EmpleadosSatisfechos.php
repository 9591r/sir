<?php
include '../forms/conexion.php'; include '../forms/funciones.php';


$sql = "SELECT DATE_FORMAT(MES_FECHA, '%Y') YYYY FROM SATISFACCION_CURSO GROUP BY DATE_FORMAT(MES_FECHA, '%Y') ORDER BY DATE_FORMAT(MES_FECHA, '%Y') DESC";

?><div class="col-md-4 col-md-offset-4"><?php
getInputSelectWhere('YYYY','YYYY', $sql ,$con);
?><input type="submit" id="boton" class='form-control' value="FILTRAR"></div><?php

if(isset($_POST['YYYY'])){
    //setJSBoxMessage($_POST['YYYY'].'asdf');
    $YYYY = " WHERE 
    DATE_FORMAT(MES_FECHA,'%Y') = '".$_POST['YYYY']."'" ;
    $and = 'AND -- '.$YYYY;
}else{
    $YYYY = '';
    $and = '';
}

$sql = "SELECT EMPLEADO.*,CONCAT(EMPLEADO.PRIMER_NOMBRE,' ',EMPLEADO.SEGUNDO_NOMBRE,' ',EMPLEADO.PRIMER_APELLIDO,' - ',PUESTO.NOMBRE_PUESTO) NOMBRE_EMPLEADO FROM EMPLEADO INNER JOIN PUESTO ON EMPLEADO.PUESTO = PUESTO.PUESTO";
$sel= $con->query($sql);

$select = "
SELECT MES.MES
";

$sql=" FROM 
(SELECT DISTINCT DATE_FORMAT(SATISFACCION_CURSO.MES_FECHA,'%Y-%m') MES
	FROM SATISFACCION_CURSO
        $YYYY
		ORDER BY MES_FECHA DESC
			LIMIT 12
) MES
";
$x = 0;
while ($fila = $sel -> fetch_assoc()) {
    $nombre[$x] = "CAMPO".$fila['EMPLEADO'];

    $select .= " , 
    CASE
    WHEN   ".$nombre[$x].".CANTIDAD".$nombre[$x]." is null THEN  'NULL'
    WHEN ".$nombre[$x].".CANTIDAD".$nombre[$x]." > 100 THEN  CONCAT(100,'')
    ELSE CONCAT(".$nombre[$x].".CANTIDAD".$nombre[$x].",'')
    END CANTIDAD".$nombre[$x];

    $sql .= "
    LEFT JOIN
    (SELECT 
    FECHA.MES
    ,CANTIDAD".$nombre[$x]."
    FROM (
        SELECT DISTINCT DATE_FORMAT(SATISFACCION_CURSO.MES_FECHA,'%Y-%m') MES
FROM SATISFACCION_CURSO
$YYYY    
    ) FECHA
    LEFT JOIN (
    SELECT
    SUM(SATISFACCION_CURSO.CANTIDAD) CANTIDAD".$nombre[$x].",
    DATE_FORMAT(SATISFACCION_CURSO.MES_FECHA,'%Y-%m') MES
        FROM SATISFACCION_CURSO 
            WHERE EMPLEADO = ".$fila['EMPLEADO']."
    GROUP BY EMPLEADO,MES_FECHA
        )  SATISFACCION_CURSO ON   SATISFACCION_CURSO.MES = FECHA.MES

    ) ".$nombre[$x]."
		ON ".$nombre[$x].".MES = MES.MES

    ";

    $arrayColores[$x] = 'RGB('.rand(0,255).','.rand(0,255).','.rand(0,255).')';
    $arrayNombres[$x] = $fila['NOMBRE_EMPLEADO'];
    $x++;
}
$order = " ORDER BY MES.MES ASC "; 
$sel= $con->query($select.$sql.$from.$order);
//echo $select.$sql.$from.$order;
$columns = 0;
while ($fila = $sel -> fetch_assoc()) {
    for($campos=0; $campos<$x; $campos++){
        $matrizValores[$campos][$columns] = $fila['CANTIDAD'.$nombre[$campos]];
    }
    $arrayColumnas[$columns] = $fila['MES'];
    $columns++;
}

$nombre = 'Materiales Fabricados';
$nombreId = 'MaterialesFabricados';
//print_r($matrizValores);
hacerGrafica($nombre,$nombreId,$arrayColumnas,$arrayNombres,$matrizValores,$arrayColores,55);
Desplegar_Tabla($nombre,'PRODUCTO',$arrayColumnas,$arrayNombres,$arrayColores,$matrizValores,55);

?>
<script>
$("#boton").click(function(){
    var input = {
        YYYY: $('[name="YYYY"]').val()
    };
    
    $.ajax({
        url: "/SIR/pages/KPI/KPI-EmpleadosSatisfechos.php",
        type: 'post',
        data: input,
        success: function(respuesta) {
            $('#Menu').html(respuesta);
        },
        error: function() {
            $('#Menu').html('ERROR EN EL FORMULARIO');
        }
    });
});
</script>