<?php
include '../forms/conexion.php'; include '../forms/funciones.php';


$sql = "SELECT DATE_FORMAT(MES_FECHA, '%Y') YYYY FROM TIEMPO_FABRICACION GROUP BY DATE_FORMAT(MES_FECHA, '%Y') ORDER BY DATE_FORMAT(MES_FECHA, '%Y') DESC";

?><div class="col-md-4 col-md-offset-4"><?php
getInputSelectWhere('YYYY','YYYY', $sql ,$con);
getInputSelect('PRODUCTO','NOMBRE_PRODUCTO', 'PRODUCTO',$con)
?><input type="submit" id="boton" class='form-control' value="FILTRAR"></div><?php

if(isset($_POST['YYYY'])){
    //setJSBoxMessage($_POST['YYYY'].'asdf');
    $YYYY = "
     WHERE DATE_FORMAT(MES_FECHA,'%Y') = '".$_POST['YYYY']."'";
    $PRODUCTO = " AND PRODUCTO = '".$_POST['PRODUCTO']."'";

    ?>
    <script>
    $('[name="PRODUCTO"]').val('<?php echo $_POST['PRODUCTO'];   ?>');
    $('[name="YYYY"]').val('<?php echo $_POST['YYYY']; ?>');
    </script>
    <?php

}else{
    $YYYY = '';
    $PRODUCTO = '';
}

$sql = "SELECT * FROM PRODUCTO";
$sel= $con->query($sql);

$sql="SELECT 
MES.MES
,MAQUINA.CANTIDAD C0
,HOMBRE.CANTIDAD C1

FROM 
(SELECT DISTINCT DATE_FORMAT(TIEMPO_FABRICACION.MES_FECHA,'%Y-%m') MES
   FROM TIEMPO_FABRICACION
   $YYYY
       ORDER BY MES_FECHA DESC
           LIMIT 12) MES
LEFT JOIN (

   SELECT DATE_FORMAT(TIEMPO_FABRICACION.MES_FECHA,'%Y-%m') MES,SUM(CANTIDAD_HORAS) CANTIDAD
   FROM TIEMPO_FABRICACION
   WHERE TIPO_FABRICACION = 1
   $PRODUCTO
    

) MAQUINA ON MAQUINA.MES = MES.MES
LEFT JOIN (

   SELECT DATE_FORMAT(TIEMPO_FABRICACION.MES_FECHA,'%Y-%m') MES,SUM(CANTIDAD_HORAS) CANTIDAD
   FROM TIEMPO_FABRICACION
   WHERE TIPO_FABRICACION = 2
   $PRODUCTO

)HOMBRE ON HOMBRE.MES = MES.MES
";
$x = 2;

$arrayColores[0] = '#765432';
$arrayColores[1] = '#fedcba';
$arrayNombres[0] = 'TIEMPO MAQUINA';
$arrayNombres[1] = 'TIEMPO MANO DE OBRA';

$sel= $con->query($sql);
//echo $sql;
$columns = 0;
while ($fila = $sel -> fetch_assoc()) {
    for($campos=0; $campos<$x; $campos++){
        $matrizValores[$campos][$columns] = $fila['C'.$campos];
    }
    $arrayColumnas[$columns] = $fila['MES'];
    $columns++;
}

$nombre = 'Horas de Fabricacion';
$nombreId = 'Horas_de_Fabricacion';
//print_r($matrizValores);
hacerGrafica($nombre,$nombreId,$arrayColumnas,$arrayNombres,$matrizValores,$arrayColores,100);
Desplegar_Tabla($nombre,'PRODUCTO',$arrayColumnas,$arrayNombres,$arrayColores,$matrizValores,100);

?>
<script>
$("#boton").click(function(){
    var input = {
        YYYY: $('[name="YYYY"]').val(),
        PRODUCTO: $('[name="PRODUCTO"]').val()
    };
    
    $.ajax({
        url: "/SIR/pages/KPI/KPI-TiempoFabricacion.php",
        type: 'post',
        data: input,
        success: function(respuesta) {
            $('#Menu').html(respuesta);
        },
        error: function() {
            $('#Menu').html('ERROR EN EL FORMULARIO');
        }
    });
});
</script>