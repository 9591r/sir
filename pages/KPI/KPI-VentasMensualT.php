<?php
include '../forms/conexion.php'; include '../forms/funciones.php';

function rand_color(){
    $colorRND = substr(md5(rand()),0, 6);
    $colorear = '#';
    return $colorear.$colorRND;
}

$sql = "SELECT DATE_FORMAT(FECHA_REGISTRO, '%Y') YYYY FROM CANTIDAD_VENTAS GROUP BY DATE_FORMAT(FECHA_REGISTRO, '%Y') ORDER BY DATE_FORMAT(FECHA_REGISTRO, '%Y') DESC";


getInputSelectWhere('YYYY','YYYY', $sql ,$con);
?><input type="submit" id="boton" value="BUSCAR"><?php

if(isset($_POST['YYYY'])){
    //setJSBoxMessage($_POST['YYYY'].'asdf');
    $YYYY = " WHERE 
    DATE_FORMAT(FECHA_REGISTRO,'%Y') = '".$_POST['YYYY']."'" ;
    $and = 'AND -- '.$YYYY;
}else{
    $YYYY = '';
    $and = '';
}

$sql = "SELECT * FROM TIPO_SOLICITUDO";
$sel= $con->query($sql);
$sql="SELECT *, MES.MES FROM 
(SELECT DISTINCT DATE_FORMAT(CANTIDAD_VENTAS.FECHA_REGISTRO,'%Y-%m') MES
	FROM CANTIDAD_VENTAS
        $YYYY
		ORDER BY FECHA_REGISTRO DESC
			LIMIT 12
) MES
";
$x = 0;
while ($fila = $sel -> fetch_assoc()) {
    $nombre[$x] = "CAMPO".$fila['ID_TIPO_SOLICITUD'];
    $sql .= "
    LEFT JOIN
    (SELECT 
    DATE_FORMAT(CANTIDAD_VENTAS.FECHA_REGISTRO,'%Y-%m') MES
    ,CASE 
    WHEN (SELECT COUNT(*) FROM CANTIDAD_VENTAS 
    WHERE DATE_FORMAT(CANTIDAD_VENTAS.FECHA_REGISTRO,'%m') >= '1'
    AND DATE_FORMAT(CANTIDAD_VENTAS.FECHA_REGISTRO,'%m') <'13'
    AND ID_TIPO_SOLICITUD = ".$fila['ID_TIPO_SOLICITUD'].") 
    IS NOT NULL THEN  
    (SELECT COUNT(*) FROM CANTIDAD_VENTAS 
    WHERE DATE_FORMAT(CANTIDAD_VENTAS.FECHA_REGISTRO,'%m') >= '1'
    AND DATE_FORMAT(CANTIDAD_VENTAS.FECHA_REGISTRO,'%m') <'13'
    AND ID_TIPO_SOLICITUD = ".$fila['ID_TIPO_SOLICITUD'].") 
    ELSE 'NULL'
    END CANTIDAD".$nombre[$x]."
    FROM CANTIDAD_VENTAS
    WHERE ID_TIPO_SOLICITUD = ".$fila['ID_TIPO_SOLICITUD']."

        GROUP BY ID_TIPO_SOLICITUD,FECHA_REGISTRO
    ) ".$nombre[$x]."
		ON ".$nombre[$x].".MES = MES.MES

    ";
    $arrayColores[$x] = rand_color();
    $arrayNombres[$x] = $fila['TIPO_SOLICITUD'];
    $x++;
}
$sel= $con->query($sql.$from);
//echo $sql.$from;
$columns = 0;
while ($fila = $sel -> fetch_assoc()) {
    for($campos=0; $campos<$x; $campos++){
        $matrizValores[$campos][$columns] = $fila['CANTIDAD'.$nombre[$campos]];
    }
    $arrayColumnas[$columns] = $fila['MES'];
    $columns++;
}

$nombre = 'Medicion Pedidos por Tipo de Soliicitud Mensual';
$nombreId = 'TipoSolicitud';
//print_r($matrizValores);
hacerGraficaBarras($nombre,$nombreId,$arrayColumnas,$arrayNombres,$matrizValores,$arrayColores,0);
Desplegar_Tabla($nombre,'TIPO_SOLICITUD',$arrayColumnas,$arrayNombres,$arrayColores,$matrizValores,0);

?>
<script>
    $("#boton").click(function(){
        var input = {
            YYYY: $('[name="Y   YYY"]').val()
        };

        $.ajax({
            url: "/SIR/pages/KPI/KPI-VentasMensualT.php",
            type: 'post',
            data: input,
            success: function(respuesta) {
                //console.log(respuesta);
                $('#Menu').html(respuesta);
            },
            error: function() {
                //console.log("No se ha podido obtener la información");
                $('#Menu').html('ERROR EN EL FORMULARIO ');
            }
        });
    });
</script>