<?php
include '../forms/conexion.php'; include '../forms/funciones.php';


$sql = "SELECT DATE_FORMAT(MES_FECHA, '%Y') YYYY FROM MATERIA_UTILIZADA GROUP BY DATE_FORMAT(MES_FECHA, '%Y') ORDER BY DATE_FORMAT(MES_FECHA, '%Y') DESC";

?><div class="col-md-4 col-md-offset-4"><?php
getInputSelectWhere('YYYY','YYYY', $sql ,$con);
?><input type="submit" id="boton" class='form-control' value="FILTRAR"></div><?php

if(isset($_POST['YYYY'])){
    //setJSBoxMessage($_POST['YYYY'].'asdf');
    $YYYY = " WHERE 
    DATE_FORMAT(MES_FECHA,'%Y') = '".$_POST['YYYY']."'" ;
    $and = 'AND -- '.$YYYY;
}else{
    $YYYY = '';
    $and = '';
}

$sql = "SELECT * FROM MATERIA";
$sel= $con->query($sql);

$select = "
SELECT MES.MES
";

$sql=" FROM 
(SELECT DISTINCT DATE_FORMAT(MATERIA_UTILIZADA.MES_FECHA,'%Y-%m') MES
	FROM MATERIA_UTILIZADA
        $YYYY
		ORDER BY MES_FECHA DESC
			LIMIT 12
) MES
";
$x = 0;
while ($fila = $sel -> fetch_assoc()) {
    $nombre[$x] = "CAMPO".$fila['PRODUCTO'];

    $select .= " , 
    CASE
    WHEN ".$nombre[$x].".CANTIDAD".$nombre[$x]." IS NOT NULL THEN  CONCAT(".$nombre[$x].".CANTIDAD".$nombre[$x].",'')
    ELSE 'NULL'
    END CANTIDAD".$nombre[$x];

    $sql .= "
    LEFT JOIN
    (SELECT 
    FECHA.MES
    ,CANTIDAD".$nombre[$x]."
    FROM (
        SELECT DISTINCT DATE_FORMAT(MATERIA_UTILIZADA.MES_FECHA,'%Y-%m') MES
FROM MATERIA_UTILIZADA
$YYYY    
    ) FECHA
    LEFT JOIN (
    SELECT
    SUM(MATERIA_UTILIZADA.CANTIDAD_UTILIZADA) CANTIDAD".$nombre[$x].",
    DATE_FORMAT(MATERIA_UTILIZADA.MES_FECHA,'%Y-%m') MES
        FROM MATERIA_UTILIZADA 
            WHERE MATERIA = ".$fila['MATERIA']."
    GROUP BY MATERIA,MES_FECHA
        )  MATERIA_UTILIZADA ON   MATERIA_UTILIZADA.MES = FECHA.MES

    ) ".$nombre[$x]."
		ON ".$nombre[$x].".MES = MES.MES

    ";
    $arrayColores[$x] = $fila['MATERIA_COLOR'];
    $arrayNombres[$x] = $fila['MATERIA_NOMBRE'];
    $x++;
}
$order = " ORDER BY MES.MES ASC "; 
$sel= $con->query($select.$sql.$from.$order);
//echo $select.$sql.$from.$order;
$columns = 0;
while ($fila = $sel -> fetch_assoc()) {
    for($campos=0; $campos<$x; $campos++){
        $matrizValores[$campos][$columns] = $fila['CANTIDAD'.$nombre[$campos]];
    }
    $arrayColumnas[$columns] = $fila['MES'];
    $columns++;
}

$nombre = 'Materiales Fabricados';
$nombreId = 'MaterialesFabricados';
//print_r($matrizValores);
hacerGrafica($nombre,$nombreId,$arrayColumnas,$arrayNombres,$matrizValores,$arrayColores,100);
Desplegar_Tabla($nombre,'PRODUCTO',$arrayColumnas,$arrayNombres,$arrayColores,$matrizValores,100);

?>
<script>
$("#boton").click(function(){
    var input = {
        YYYY: $('[name="YYYY"]').val()
    };
    
    $.ajax({
        url: "/SIR/pages/KPI/KPI-ProductosFabricados.php",
        type: 'post',
        data: input,
        success: function(respuesta) {
            $('#Menu').html(respuesta);
        },
        error: function() {
            $('#Menu').html('ERROR EN EL FORMULARIO');
        }
    });
});
</script>