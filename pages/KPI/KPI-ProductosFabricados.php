<?php
include '../forms/conexion.php'; include '../forms/funciones.php';


$sql = "SELECT DATE_FORMAT(MES_FECHA, '%Y') YYYY FROM PRODUCTO_FABRICADO GROUP BY DATE_FORMAT(MES_FECHA, '%Y') ORDER BY DATE_FORMAT(MES_FECHA, '%Y') DESC";

?><div class="col-md-4 col-md-offset-4"><?php
getInputSelectWhere('YYYY','YYYY', $sql ,$con);
?><input type="submit" id="boton" class='form-control' value="FILTRAR"></div><?php

if(isset($_POST['YYYY'])){
    //setJSBoxMessage($_POST['YYYY'].'asdf');
    $YYYY = " WHERE 
    DATE_FORMAT(MES_FECHA,'%Y') = '".$_POST['YYYY']."'" ;
    $and = 'AND -- '.$YYYY;
}else{
    $YYYY = '';
    $and = '';
}

$sql = "SELECT * FROM PRODUCTO";
$sel= $con->query($sql);

$select = "
SELECT MES.MES
";

$sql=" FROM 
(SELECT DISTINCT DATE_FORMAT(PRODUCTO_FABRICADO.MES_FECHA,'%Y-%m') MES
	FROM PRODUCTO_FABRICADO
        $YYYY
		ORDER BY MES_FECHA DESC
			LIMIT 12
) MES
";
$x = 0;
while ($fila = $sel -> fetch_assoc()) {
    $nombre[$x] = "CAMPO".$fila['PRODUCTO'];

    $select .= " , 
    CASE
    WHEN ".$nombre[$x].".CANTIDAD".$nombre[$x]." IS NOT NULL THEN  CONCAT(".$nombre[$x].".CANTIDAD".$nombre[$x].",'')
    ELSE 'NULL'
    END CANTIDAD".$nombre[$x];

    $sql .= "
    LEFT JOIN
    (SELECT 
    FECHA.MES
    ,CANTIDAD".$nombre[$x]."
    FROM (
        SELECT DISTINCT DATE_FORMAT(PRODUCTO_FABRICADO.MES_FECHA,'%Y-%m') MES
FROM PRODUCTO_FABRICADO
$YYYY    
    ) FECHA
    LEFT JOIN (
    SELECT
    SUM(PRODUCTO_FABRICADO.CANTIDAD_FABRICADO) CANTIDAD".$nombre[$x].",
    DATE_FORMAT(PRODUCTO_FABRICADO.MES_FECHA,'%Y-%m') MES
        FROM PRODUCTO_FABRICADO 
            WHERE PRODUCTO = ".$fila['PRODUCTO']."
    GROUP BY PRODUCTO,MES_FECHA
        )  PRODUCTO_FABRICADO ON   PRODUCTO_FABRICADO.MES = FECHA.MES

    ) ".$nombre[$x]."
		ON ".$nombre[$x].".MES = MES.MES

    ";
    $arrayColores[$x] = $fila['COLOR_PRODUCTO'];
    $arrayNombres[$x] = $fila['NOMBRE_PRODUCTO'];
    $x++;
}
$order = " ORDER BY MES.MES ASC "; 
$sel= $con->query($select.$sql.$from.$order);
//echo $select.$sql.$from.$order;
$columns = 0;
while ($fila = $sel -> fetch_assoc()) {
    for($campos=0; $campos<$x; $campos++){
        $matrizValores[$campos][$columns] = $fila['CANTIDAD'.$nombre[$campos]];
    }
    $arrayColumnas[$columns] = $fila['MES'];
    $columns++;
}

$nombre = 'Producto Fabricados';
$nombreId = 'ProductoFabricados';
//print_r($matrizValores);
hacerGrafica($nombre,$nombreId,$arrayColumnas,$arrayNombres,$matrizValores,$arrayColores,100);
Desplegar_Tabla($nombre,'PRODUCTO',$arrayColumnas,$arrayNombres,$arrayColores,$matrizValores,100);

?>
<script>
$("#boton").click(function(){
    var input = {
        YYYY: $('[name="YYYY"]').val()
    };
    
    $.ajax({
        url: "/SIR/pages/KPI/KPI-ProductosFabricados.php",
        type: 'post',
        data: input,
        success: function(respuesta) {
            $('#Menu').html(respuesta);
        },
        error: function() {
            $('#Menu').html('ERROR EN EL FORMULARIO');
        }
    });
});
</script>