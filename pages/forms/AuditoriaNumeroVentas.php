<?php include 'conexion.php'; include 'funciones.php';  ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Ingreso de Venta
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Inicio Seccion del area de la tabla -->
        <section class="col-lg-6 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                    <FORM ACTION="#" METHOD="post" id="myform">
                        <div class="form-group">
                            <br><label for="Puesto">Codigo Venta</label>
                            <input type="text" name="idCantidadVentas"  id="idCantidadVentas" class="form-control" readonly>
                            <label for="Puesto">MATERIA PRIMA</label>
                            <?php getInputSelect('ID_MATERIA_PRIMA','NOMBRE_MATERIA_PRIMA','MATERIAS_PRIMAS',$con); ?>
                            <br><label for="Puesto">Cantidad de Unidades de Materia Prima</label>
                            <input type="number" name="CantidadMateria"  id="CantidadMateria" class="form-control">
                            <label for="Puesto"><br>PRODUCTO FABRICADO</label>
                            <?php getInputSelect('PRODUCTO','NOMBRE_PRODUCTO','PRODUCTO',$con); ?>
                            <br><label for="Puesto">Cantidad de Unidades de Producto Fabricado</label>
                            <input type="number" name="CantidadProducto"  id="CantidadProducto" class="form-control">
                            <label for="Puesto"><br>CLIENTE SOLICITANTE</label>
                            <?php getInputSelect('ID_CLIENTE','NOMBRE_CLIENTE','CLIENTES',$con); ?>
                            <br>
                            <label for="Puesto"><br>TIPO SOLICITUD</label>
                            <?php getInputSelect('ID_TIPO_SOLICITUD','TIPO_SOLICITUD','TIPO_SOLICITUDO',$con); ?>
                            <br>
                            <label for="Puesto"><br>VENDEDOR</label>
                            <?php getInputSelect('EMPLEADO','PRIMER_NOMBRE','EMPLEADO',$con); ?>
                            <br><label for="Puesto">MONTO_VENTA</label>
                            <input type="number" name="Monto"  id="Monto" class="form-control" min="0.00" max="10000.00" step="0.01">
                        </div>
                        <div class="form-group">
                            <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger">
                            <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger" onclick="guardandoOK()">
                        </div>
                    </FORM>
                </div>
            </div>
        </section>
        <section class="col-lg-7 connectedSortable" style="width: 950px">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="form-group">
                    <table class="table table-bordered">
                        <caption>
                        </caption>
                        <tr>
                            <td>CODIGO</td>
                            <td>MATERIA PRIMA</td>
                            <td>CANTIDAD MATERIA PRIMA</td>
                            <td>PRODUCTO FABRICADO</td>
                            <td>CANTIDAD PRODUCTO FABRICADO</td>
                            <td>CLIENTE</td>
                            <td>TIPO DE SOLICITUD</td>
                            <td>VENDEDOR</td>
                            <td>PRECIO</td>
                            <td>FECHA REGISTRO</td>
                            <td>Editar</td>
                            <td>Eliminar</td>
                        </tr>
                        <?php
                        $sel= $con->query("SELECT
    CV.ID_CANTIDAD_VENTAS,
    MP.NOMBRE_MATERIA_PRIMA,
    CV.CANTIDAD_MATERIA,
    P.NOMBRE_PRODUCTO,
    CV.CANTIDAD_PRODUCTO,
    C.NOMBRE_CLIENTE,
    TS.TIPO_SOLICITUD,
    E.PRIMER_NOMBRE,
    CV.MONTO_VENTA,
    CV.FECHA_REGISTRO
FROM
    cantidad_ventas CV
INNER JOIN
	materias_primas MP
ON
	CV.ID_MATERIA_PRIMA = MP.ID_MATERIA_PRIMA
INNER JOIN
	producto P 
ON
	CV.ID_PRODUCTO_FABRICADO = P.PRODUCTO
INNER JOIN
	clientes C 
ON	
	CV.ID_CLIENTE = C.ID_CLIENTE
INNER JOIN
	tipo_solicitudo TS 
ON	
	CV.ID_TIPO_SOLICITUD = TS.ID_TIPO_SOLICITUD
INNER JOIN
	empleado E 
ON	
	CV.ID_EMPLEADO = E.EMPLEADO
ORDER BY
	CV.FECHA_REGISTRO
DESC;");
                        while ($fila = $sel -> fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $fila['ID_CANTIDAD_VENTAS'] ?></td>
                                <td><?php echo $fila['NOMBRE_MATERIA_PRIMA'] ?></td>
                                <td><?php echo $fila['CANTIDAD_MATERIA'] ?></td>
                                <td><?php echo $fila['NOMBRE_PRODUCTO'] ?></td>
                                <td><?php echo $fila['CANTIDAD_PRODUCTO'] ?></td>
                                <td><?php echo $fila['NOMBRE_CLIENTE'] ?></td>
                                <td><?php echo $fila['TIPO_SOLICITUD'] ?></td>
                                <td><?php echo $fila['PRIMER_NOMBRE'] ?></td>
                                <td><?php echo $fila['MONTO_VENTA'] ?></td>
                                <td><?php echo $fila['FECHA_REGISTRO'] ?></td>
                                <td>
                                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion"
                                            onclick="editform('<?php echo $fila['ID_CANTIDAD_VENTAS'] ?>'
                                    ,'<?php echo $fila['ID_MATERIA_PRIMA'] ?>'
                                    ,'<?php echo $fila['CANTIDAD_MATERIA'] ?>'
                                    ,'<?php echo $fila['PRODUCTO'] ?>'
                                    ,'<?php echo $fila['CANTIDAD_PRODUCTO'] ?>'
                                    ,'<?php echo $fila['ID_CLIENTE'] ?>'
                                    ,'<?php echo $fila['ID_TIPO_SOLICITUD'] ?>'
                                    ,'<?php echo $fila['EMPLEADO'] ?>'
                                    ,'<?php echo $fila['MONTO_VENTA'] ?>')">
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-danger glyphicon glyphicon-remove"
                                            onclick="preguntarSiNo('<?php echo $fila['ID_CANTIDAD_VENTAS'] ?>'
                                                    ,'<?php echo $fila['ID_MATERIA_PRIMA'] ?>'
                                                    ,'<?php echo $fila['CANTIDAD_MATERIA'] ?>'
                                                    ,'<?php echo $fila['PRODUCTO'] ?>'
                                                    ,'<?php echo $fila['CANTIDAD_PRODUCTO'] ?>'
                                                    ,'<?php echo $fila['ID_CLIENTE'] ?>'
                                                    ,'<?php echo $fila['ID_TIPO_SOLICITUD'] ?>'
                                                    ,'<?php echo $fila['EMPLEADO'] ?>'
                                                    ,'<?php echo $fila['MONTO_VENTA'] ?>')">
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </section>
    </div>
</section>
<script>
    $(function () {
        $('#myform').prop('action', './pages/forms/InsertarVenta.php');
        $('.textarea').wysihtml5()
    })
    function editform(id, nmp, cm, np, cp, nc, ts, ne, precio){
        $("#idCantidadVentas").val(id);
        $("#ID_MATERIA_PRIMA").val(nmp);
        $("#CantidadMateria").val(cm);
        $("#PRODUCTO").val(np);
        $("#CantidadProducto").val(cp);
        $("#ID_CLIENTE").val(nc);
        $("#ID_TIPO_SOLICITUD").val(ts);
        $("#ID_EMPLEADO").val(ne);
        $("#Monto").val(precio);
        $('#myform').prop('action', './pages/forms/ModificarVenta.php');
    }
    function preguntarSiNo(id, nmp, cm, np, cp, nc, ts, ne, precio){
        $("#idCantidadVentas").val(id);
        $("#ID_MATERIA_PRIMA").val(nmp);
        $("#CantidadMateria").val(cm);
        $("#PRODUCTO").val(np);
        $("#CantidadProducto").val(cp);
        $("#ID_CLIENTE").val(nc);
        $("#ID_TIPO_SOLICITUD").val(ts);
        $("#ID_EMPLEADO").val(ne);
        $("#Monto").val(precio);
        $('#myform').prop('action', './pages/forms/EliminarVenta.php');
    }
    function guardandoOK() {
        $('#myform').prop('action', './pages/forms/InsertarVenta.php');
        $('.textarea').wysihtml5()
    }
</script>