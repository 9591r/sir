<?php include 'conexion.php'; ?>

    <section class="content-header">
      <h1>
        RESUMEN DE LA AUDITORIA INTERNA
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content"><!-- INICIO SECCION CONTENT -->

      <FORM ACTION="InsertarAudResumen.php" METHOD="post"><!-- INICIO FORM -->
    
      <div class="row">
      
         <!-- INICIO SECCION SCOPE OF AUDITORIA INTERNA -->
        <section class="col-lg-4 connectedSortable">
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">

                <label for="Auditoria Interna">Auditoria Interna</label>
                
                <div class="form-group">
                <label for="AIFechaInicio">Fecha Inicio:</label>
                <input type="AIFechaInicio" name="AIFechaInicio" class="form-control">  
                <label for="AIFechaFin">Fecha Fin:</label>
                <input type="AIFechaFin" name="AIFechaFin" class="form-control">           
                
                <label for="DireccionUno">Direccion</label>
                <input type="DireccionUno" name="DireccionUno" class="form-control">            

                <label for="Ubicacion">Ubicacion</label>
                <input type="Ubicacion" name="Ubicacion" class="form-control">       

                </div>
                
              </div>
            </div>

            <!-- INICIO SECCION SCOPE OF AUDIT -->
            <section class="col-lg-13 connectedSortable">
                <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                
                <label for="Scope">Scope of audit</label>
                <textarea name="Scope" class="form-control" rows="3"></textarea>

                <label for="HallazgosUno">Hallazgos</label>
                <textarea class="form-control" rows="7" name="HallazgosUno"></textarea>

                <label for="HallazgosDos">Hallazgos</label>
                <textarea class="form-control" rows="7" name="HallazgosDos"></textarea>

                <div>
                  <label> </label>
                </div>

                <div class="form-group">
                <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger"> 
                <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger">
                </div>  
               
                </div>
                </div> 

            </section> <!-- FIN SECCION SCOPE OF AUDIT -->

          </section> <!-- FIN SECCION SCOPE OF AUDITORIA INTERNA -->

           <!-- INICIO SECCION REUNION CIERRE -->
          <section class="col-lg-4 connectedSortable">
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
                              
                <label for="Auditoria Interna">Reunion de Cierre</label>
                
                <div class="form-group">
                <label for="RCFechaInicio">Fecha Inicio:</label>
                <input type="RCFechaInicio" name="RCFechaInicio" class="form-control">   

                <label for="RCFechaFin">Fecha Fin:</label>
                <input type="RCFechaFin" name="RCFechaFin" class="form-control">         
               
                <label for="DireccionDos">Direccion</label>
                <input type="DireccionDos" name="DireccionDos" class="form-control">            
               
              </div>
            </div>
          </div>
          <!-- INICIO SECCION SCOPE OF AUDIT -->
            <section class="col-lg-13 connectedSortable">
                <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">

                <div class="form-group">

                  <div class="form-group">
                  <label for="sel1">Lider</label>
                  <select class="form-control" id="sel1" name="Lider">
                  <?php 
                  $sel= $con->query("SELECT * FROM Empleados WHERE IDPuesto ='6'");
                  while ($fila = $sel -> fetch_assoc()) {
                  ?>
                  <option><?php echo $fila['IDEmp'] ?></option>
                  <?php } ?>
                  </select> 
                  </div>
                 
                </div>         

                <div class="form-group">

                  <div class="form-group">
                  <label for="sel1">Auditor 1</label>
                  <select class="form-control" id="sel1" name="AudUno">
                  <?php 
                  $sel= $con->query("SELECT * FROM Empleados WHERE IDPuesto ='6'");
                  while ($fila = $sel -> fetch_assoc()) {
                  ?>
                  <option><?php echo $fila['IDEmp'] ?></option>
                  <?php } ?>
                  </select> 
                  </div>
                 
                </div>          

                <div class="form-group">

                  <div class="form-group">
                  <label for="sel1">Auditor 2</label>
                  <select class="form-control" id="sel1" name="AudDos">
                  <?php 
                  $sel= $con->query("SELECT * FROM Empleados WHERE IDPuesto ='6'");
                  while ($fila = $sel -> fetch_assoc()) {
                  ?>
                  <option><?php echo $fila['IDEmp'] ?></option>
                  <?php } ?>
                  </select> 
                  </div>
                 
                </div>       

                <div class="form-group">

                  <div class="form-group">
                  <label for="sel1">En Entrenamiento</label>
                  <select class="form-control" id="sel1" name="Entrenamiento">
                  <?php 
                  $sel= $con->query("SELECT * FROM Empleados WHERE IDPuesto ='6'");
                  while ($fila = $sel -> fetch_assoc()) {
                  ?>
                  <option><?php echo $fila['IDEmp'] ?></option>
                  <?php } ?>
                  </select> 
                  </div>
                 
                </div>       
                
                <label for="Proceso">Procesos</label>
                <textarea class="form-control" rows="3" name="Proceso"></textarea>

                <label for="Procesos">Referenciar documentos usados durante está auditoria :</label>

                <div class="checkbox active">
                  <label><input type="checkbox" name="ckMCalidad" value="1">Manual de Calidad</label>
                </div>
                <div class="checkbox active">
                  <label><input type="checkbox" name="ckISO" value="1">ISO 9001-2008</label>
                </div>
                <div class="checkbox active">
                  <label><input type="checkbox" name="ckProce" value="1">Procedimientos</label>
                </div>
                <div class="checkbox active">
                  <label><input type="checkbox" name="ckInsTrabajo" value="1">Instrucciones de Trabajo</label>
                </div>
                <div class="checkbox active">
                  <label><input type="checkbox" name="ckForm" value="1">Formularios</label>
                </div>
                <div class="checkbox active">
                  <label><input type="checkbox" name="checklist" value="1">Check List</label>
                </div>
              
                </div>
                </div> 

            </section> <!-- FIN SECCION SCOPE OF AUDIT -->
          </section> 
          <!-- FIN SECCION REUNION CIERRE -->

      </FORM>   <!-- Fin FORM -->

    </section> <!-- FIN SECCION CONTENT -->