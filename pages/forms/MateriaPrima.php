<?php include 'conexion.php'; ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Materias Primas
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- Inicio Seccion del area de la tabla -->

        <section class="col-lg-6 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">

                    <FORM ACTION="#" METHOD="post" id="myform">

                        <div class="form-group">
                            <label for="Materia">Materia Prima</label>
                            <input type="text" name="idMateria"  id="idMateria" class="form-control"  style="visibility:hidden">
                            <input type="text" name="MateriaPrima"  id="MateriaPrima" class="form-control">


                        </div>

                        <div class="form-group">
                            <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger">
                            <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger">
                        </div>

                    </FORM>

                </div>
            </div>
        </section>

        <section class="col-lg-7 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">

                <div class="form-group">
                    <table class="table table-bordered">

                        <caption>
                        </caption>

                        <tr>
                            <td>ID Puesto</td>
                            <td>Puesto</td>
                            <td>Editar</td>
                            <td>Eliminar</td>
                        </tr>

                        <?php

                        $sel= $con->query("SELECT * FROM MATERIAS_PRIMAS");
                        while ($fila = $sel -> fetch_assoc()) {
                            ?>

                            <tr>
                                <td><?php echo $fila['ID_MATERIA_PRIMA'] ?></td>
                                <td><?php echo $fila['NOMBRE_MATERIA_PRIMA'] ?></td>
                                <td>
                                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion"
                                            onclick="editform('<?php echo $fila['ID_MATERIA_PRIMA'] ?>', '<?php echo $fila['NOMBRE_MATERIA_PRIMA'] ?>')">
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-danger glyphicon glyphicon-remove"
                                            onclick="preguntarSiNo('<?php echo $fila['ID_MATERIA_PRIMA'] ?>', '<?php echo $fila['NOMBRE_MATERIA_PRIMA'] ?>')">
                                    </button>
                                </td>
                            </tr>

                        <?php } ?>

                    </table>

                </div>
            </div>
        </section>


    </div>
    <!-- Fin div del area de contenido -->

</section>

<script>
    $(function () {
        $('#myform').prop('action', './pages/forms/InsertarMateriaPrima.php');
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })

    function editform(id, nombre){
        $("#idMateria").val(id);
        $("#MateriaPrima").val(nombre);
        $('#myform').prop('action', './pages/forms/ModificarMateriaPrima.php');
        //alert("modifica");
    }
    function preguntarSiNo(id, nombre){
        $("#idMateria").val(id);
        $("#MateriaPrima").val(nombre);
        $('#myform').prop('action', './pages/forms/EliminarMateriaPrima.php');
        //alert("eliminar");
    }
</script>