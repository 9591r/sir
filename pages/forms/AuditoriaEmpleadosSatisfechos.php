<?php include 'conexion.php'; include 'funciones.php';  ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Creación de Puestos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">

        <!-- Inicio Seccion del area de la tabla -->

        <section class="col-lg-6 connectedSortable">
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              
              <FORM ACTION="#" METHOD="post" id="myform"> 
               
                <div class="form-group">
                <input type="Puesto" name="idPuesto"  id="idPuesto" class="form-control"  style="visibility:hidden"> 
                <label for="Puesto">Empleado</label>
                <?php $sql = "SELECT EMPLEADO, CONCAT(PRIMER_NOMBRE,' ',SEGUNDO_NOMBRE,' ',PRIMER_APELLIDO,' - ',NOMBRE_PUESTO) NOMBRE_EMPLEADO FROM EMPLEADO INNER JOIN PUESTO ON EMPLEADO.PUESTO = PUESTO.PUESTO"; 
                getInputSelectWhere('EMPLEADO','NOMBRE_EMPLEADO', $sql ,$con); ?>
                <br><label for="Puesto">Satisfaccion del Empleado por la Capacitacion. De 0 a 100</label>
                <input type="number" name="Cantidad"  id="Cantidad" min="0" min="100" class="form-control">            
                <label for="Puesto">Mes Fabricado</label>
                <input type="month" name="Mes"  id="Mes" class="form-control">
                <label for="Puesto">Comentario</label>
                <input type="textbox" name="Comentario"  id="Mes" class="form-control">                             
                
                </div>
                
                <div class="form-group">
                <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger"> 
                <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger">
                </div>

              </FORM> 

              </div>
            </div>
          </section>

           <section class="col-lg-7 connectedSortable">
              <div class="box box-solid bg-light-blue-gradient">

                  <div class="form-group">
                    <table class="table table-bordered">

                    <caption>
                  </caption>

                    <tr>
                    <td>Cantidad</td>
                    <td>EMPLEADO</td>
                    <td>Mes-Año</td>
                    <td>COMENTARIO</td>
                  </tr>

                   <?php
        
                    $sel= $con->query("SELECT 
                  SATISFACCION_CURSO.CANTIDAD
                 ,SATISFACCION_CURSO.COMENTARIO
                 ,DATE_FORMAT(SATISFACCION_CURSO.MES_FECHA,'%m-%Y') MES_FECHA  
                 ,CONCAT(EMPLEADO.PRIMER_NOMBRE,' ',EMPLEADO.SEGUNDO_NOMBRE,' ',EMPLEADO.PRIMER_APELLIDO,' - ',PUESTO.NOMBRE_PUESTO) NOMBRE_EMPLEADO
                        FROM SATISFACCION_CURSO 
                            INNER JOIN EMPLEADO
                                ON SATISFACCION_CURSO.EMPLEADO = EMPLEADO.EMPLEADO
                                    INNER JOIN PUESTO
                                        ON PUESTO.PUESTO = EMPLEADO.PUESTO
                     order by SATISFACCION_CURSO.MES_FECHA DESC");
                    while ($fila = $sel -> fetch_assoc()) {
                  ?>

                   <tr>
                    <td><?php echo $fila['CANTIDAD'] ?></td>
                    <td><?php echo $fila['NOMBRE_EMPLEADO'] ?></td>
                    <td><?php echo $fila['MES_FECHA'] ?></td>
                    <td><?php echo $fila['COMENTARIO'] ?></td>
                  </tr>

                   <?php } ?>

                   </table>

              </div>
            </div>
          </section>

          <!-- Seccion del area de la tabla -->


          <!-- Inicio de la seccion de los botones -->

          <!--

          <section class="col-lg-5 connectedSortable">
          <!-- Map box -->
          <!--
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              <form>
                <div>
                <label for="email">80% a 100% -- Excelente</label>
                </div>

                <div>
                <label for="email">50% a 79% -- Bueno</label>
                </div>

                <div>
                <label for="email">0% a 49% -- Malo</label>
                </div>
                <button type="button" class="btn btn-info btn-lg btn-block"><a href="N-SoporteLocal.html"> <--- REGRESAR </button>
                <button type="button" class="btn btn-danger btn-lg btn-block">KPI Empaque Adecuado del Inventario</button>
              </form> 
          </div>

        </section>
          -->
        <!-- Fin de la seccion de los botones -->

        </div> 
        <!-- Fin div del area de contenido -->

    </section>

<script>
  $(function () {
     $('#myform').prop('action', './pages/forms/InsertarAuditoriaEmpleadosSatisfechos.php');
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  function editform(id, nombre){
     $("#idPuesto").val(id);
     $("#Puesto").val(nombre);
      $('#myform').prop('action', 'ModificarPuestos.php');
    //alert("modifica");
  }
  function preguntarSiNo(id, nombre){
    $("#idPuesto").val(id);
     $("#Puesto").val(nombre);
     $('#myform').prop('action', 'EliminarPuestos.php');
    //alert("eliminar");
  }
</script>