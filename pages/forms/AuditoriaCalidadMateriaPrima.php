<?php include 'conexion.php'; include 'funciones.php';  ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Calidad de Materia Prima
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Inicio Seccion del area de la tabla -->
        <section class="col-lg-6 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                    <FORM ACTION="#" METHOD="post" id="myform">
                        <div class="form-group">
                            <br><label for="Puesto">Codigo</label>
                            <input type="text" name="idPedido"  id="idPedido" class="form-control" readonly>
                            <label for="Puesto"><br>Materia Prima</label>
                            <?php getInputSelect('ID_MATERIA_PRIMA','NOMBRE_MATERIA_PRIMA','MATERIAS_PRIMAS',$con); ?>
                            <label for="Puesto">PROVEEDOR</label>
                            <?php getInputSelect('ID_PROVEEDOR','PROVEEDOR','PROVEEDORES',$con); ?>
                            <label for="Puesto"><br>TIPO DE CALIDAD</label>
                            <?php getInputSelect('ID_LIMPIEZA','TIPO_LIMPIEZA','LIMPIEZA',$con); ?>
                            <label for="Puesto"><br>SOLICITANTE</label>
                            <?php getInputSelect('EMPLEADO','PRIMER_NOMBRE','EMPLEADO',$con); ?>
                        </div>
                        <div class="form-group">
                            <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger">
                            <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger" onclick="guardandoOK()">
                        </div>
                    </FORM>
                </div>
            </div>
        </section>
        <section class="col-lg-7 connectedSortable" style="width: 950px">
            <div class="box box-solid bg-light-blue-gradient ">
                <div class="form-group">
                    <table class="table table-bordered">
                        <caption>
                        </caption>
                        <tr>
                            <td>CODIGO</td>
                            <td>MATERIA PRIMA</td>
                            <td>PROVEEDOR</td>
                            <td>TIPO DE CALIDAD</td>
                            <td>SOLICITANTE</td>
                            <td>FECHA REGISTRO</td>
                            <td>Editar</td>
                            <td>Eliminar</td>
                        </tr>
                        <?php
                        $sel= $con->query("SELECT
    PIN.ID_CALIDAD_SELECCION,
    MP.NOMBRE_MATERIA_PRIMA,
    PR.PROVEEDOR,
    TL.TIPO_LIMPIEZA,
    E.PRIMER_NOMBRE,
    PIN.FECHA_REGISTRO
FROM
    calidad_materia_prima PIN
INNER JOIN
	MATERIAS_PRIMAS MP
ON
	PIN.ID_MATERIA_PRIMA = MP.ID_MATERIA_PRIMA
INNER JOIN
	PROVEEDORES PR
ON
	PIN.ID_PROVEEDOR = PR.ID_PROVEEDOR
INNER JOIN
	LIMPIEZA TL 
ON	
	PIN.ID_LIMPIEZA = TL.ID_LIMPIEZA
INNER JOIN
	empleado E 
ON	
	PIN.ID_EMPLEADO = E.EMPLEADO
ORDER BY
	PIN.FECHA_REGISTRO
DESC;");
                        while ($fila = $sel -> fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $fila['ID_CALIDAD_SELECCION'] ?></td>
                                <td><?php echo $fila['NOMBRE_MATERIA_PRIMA'] ?></td>
                                <td><?php echo $fila['TIPO_LIMPIEZA'] ?></td>
                                <td><?php echo $fila['PRIMER_NOMBRE'] ?></td>
                                <td><?php echo $fila['FECHA_REGISTRO'] ?></td>
                                <td>
                                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion"
                                            onclick="editform('<?php echo $fila['ID_CALIDAD_SELECCION'] ?>'
                                                    ,'<?php echo $fila['NOMBRE_MATERIA_PRIMA'] ?>'
                                                    ,'<?php echo $fila['TIPO_LIMPIEZA'] ?>'
                                                    ,'<?php echo $fila['PRIMER_NOMBRE'] ?>')">
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-danger glyphicon glyphicon-remove"
                                            onclick="preguntarSiNo('<?php echo $fila['ID_CALIDAD_SELECCION'] ?>'
                                                    ,'<?php echo $fila['NOMBRE_MATERIA_PRIMA'] ?>'
                                                    ,'<?php echo $fila['TIPO_LIMPIEZA'] ?>'
                                                    ,'<?php echo $fila['PRIMER_NOMBRE'] ?>')">
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </section>
    </div>
</section>
<script>
    $(function () {
        $('#myform').prop('action', './pages/forms/InsertarPedidoE.php');
        $('.textarea').wysihtml5()
    })
    function editform(id, p, ne, tr){
        $("#idPedido").val(id);
        $("#ID_PROVEEDOR").val(p);
        $("#ID_EMPLEADO").val(ne);
        $("#TiempoSolicitud").val(tr);
        $('#myform').prop('action', './pages/forms/ModificarPedidoE.php');
    }
    function preguntarSiNo(id, p, ne, tr){
        $("#idPedido").val(id);
        $("#ID_PROVEEDOR").val(p);
        $("#ID_EMPLEADO").val(ne);
        $("#TiempoSolicitud").val(tr);
        $('#myform').prop('action', './pages/forms/EliminarPedidoE.php');
    }
    function guardandoOK() {
        $('#myform').prop('action', './pages/forms/InsertarPedidoE.php');
        $('.textarea').wysihtml5()
    }
</script>