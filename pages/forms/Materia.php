<?php include 'conexion.php'; ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Materia Prima
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">

        <!-- Inicio Seccion del area de la tabla -->

        <section class="col-lg-6 connectedSortable">
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              
              <FORM ACTION="#" METHOD="post" id="myform"> 
               
                <div class="form-group">
                <label for="Puesto">Color</label>
                <input type="color" name="Color"  id="Color" class="form-control">            
                <label for="Puesto">Nombre de Materia Prima</label>
                <input type="text" name="idPuesto"  id="idNombre" class="form-control"  style="visibility:hidden"> 
                <input type="text" name="Nombre"  id="Nombre" class="form-control">            
                        
                
                </div>
                
                <div class="form-group">
                <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger"> 
                <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger">
                </div>

              </FORM> 

              </div>
            </div>
          </section>

           <section class="col-lg-7 connectedSortable">
              <div class="box box-solid bg-light-blue-gradient">

                  <div class="form-group">
                    <table class="table table-bordered">

                    <caption>
                  </caption>

                    <tr>
                    <td>Color</td>
                    <td>ID Materia</td>
                    <td>Nombre Materia</td>
                    <td>Editar</td>
                    <td>Eliminar</td>
                  </tr>

                   <?php
        
                    $sel= $con->query("SELECT * FROM Materia");
                    while ($fila = $sel -> fetch_assoc()) {
                  ?>

                   <tr>
                    <td><input type="color" value="<?php echo $fila['MATERIA_COLOR'] ?>" disabled="true" ></td>
                    <td><?php echo $fila['MATERIA'] ?></td>
                    <td><?php echo $fila['MATERIA_NOMBRE'] ?></td>
                    <td>
                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion" onclick="editform('<?php echo $fila['MATERIA'] ?>', '<?php echo $fila['MATERIA_NOMBRE'] ?>', '<?php echo $fila['MATERIA_COLOR'] ?>')">
                    </button>
                    </td>
                    <td>
                    <button class="btn btn-danger glyphicon glyphicon-remove" 
                    onclick="preguntarSiNo('<?php echo $fila['MATERIA'] ?>', '<?php echo $fila['MATERIA_NOMBRE'] ?>', '<?php echo $fila['MATERIA_COLOR'] ?>')">
                    </button>
                    </td>
                  </tr>

                   <?php } ?>

                   </table>

              </div>
            </div>
          </section>

        </div> 
        <!-- Fin div del area de contenido -->

    </section>

<script>
  $(function () {
     $('#myform').prop('action', './pages/forms/InsertarMateria.php');
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  function editform(id, nombre, color){
     $("#idNombre").val(id);
     $("#Nombre").val(nombre);
     $("#Color").val(color);
      $('#myform').prop('action', './pages/forms/ModificarMateria.php');
    //alert("modifica");
  }
  function preguntarSiNo(id, nombre,color){
    $("#idNombre").val(id);
     $("#Nombre").val(nombre);
     $("#Color").val(color);
     $('#myform').prop('action', './pages/forms/EliminarMateria.php');
    //alert("eliminar");
  }
</script>