<?php include 'conexion.php'; ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Productos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">

        <!-- Inicio Seccion del area de la tabla -->

        <section class="col-lg-6 connectedSortable">
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              
              <FORM ACTION="#" METHOD="post" id="myform"> 
               
                <div class="form-group">
                <label for="Puesto">Color</label>
                <input type="color" name="Color"  id="Color" class="form-control">            
                <label for="Puesto">Nombre de Producto Fabricado</label>
                <input type="text" name="idPuesto"  id="idNombre" class="form-control"  style="visibility:hidden"> 
                <input type="text" name="Puesto"  id="Nombre" class="form-control">            
                        
                
                </div>
                
                <div class="form-group">
                <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger"> 
                <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger">
                </div>

              </FORM> 

              </div>
            </div>
          </section>

           <section class="col-lg-7 connectedSortable">
              <div class="box box-solid bg-light-blue-gradient">

                  <div class="form-group">
                    <table class="table table-bordered">

                    <caption>
                  </caption>

                    <tr>
                    <td>Color</td>
                    <td>ID Producto</td>
                    <td>Nombre Producto</td>
                    <td>Editar</td>
                    <td>Eliminar</td>
                  </tr>

                   <?php
        
                    $sel= $con->query("SELECT * FROM PRODUCTO");
                    while ($fila = $sel -> fetch_assoc()) {
                  ?>

                   <tr>
                    <td><input type="color" value="<?php echo $fila['COLOR_PRODUCTO'] ?>" disabled="true" ></td>
                    <td><?php echo $fila['PRODUCTO'] ?></td>
                    <td><?php echo $fila['NOMBRE_PRODUCTO'] ?></td>
                    <td>
                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion" onclick="editform('<?php echo $fila['PRODUCTO'] ?>', '<?php echo $fila['NOMBRE_PRODUCTO'] ?>', '<?php echo $fila['COLOR_PRODUCTO'] ?>')">
                    </button>
                    </td>
                    <td>
                    <button class="btn btn-danger glyphicon glyphicon-remove" 
                    onclick="preguntarSiNo('<?php echo $fila['PRODUCTO'] ?>', '<?php echo $fila['NOMBRE_PRODUCTO'] ?>', '<?php echo $fila['COLOR_PRODUCTO'] ?>')">
                    </button>
                    </td>
                  </tr>

                   <?php } ?>

                   </table>

              </div>
            </div>
          </section>

        </div> 
        <!-- Fin div del area de contenido -->

    </section>

<script>
  $(function () {
     $('#myform').prop('action', './pages/forms/InsertarProductos.php');
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  function editform(id, nombre, color){
     $("#idNombre").val(id);
     $("#Nombre").val(nombre);
     $("#Color").val(color);
      $('#myform').prop('action', './pages/forms/ModificarProducto.php');
    //alert("modifica");
  }
  function preguntarSiNo(id, nombre,color){
    $("#idNombre").val(id);
     $("#Nombre").val(nombre);
     $("#Color").val(color);
     $('#myform').prop('action', './pages/forms/EliminarProducto.php');
    //alert("eliminar");
  }
</script>