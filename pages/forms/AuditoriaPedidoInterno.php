<?php include 'conexion.php'; include 'funciones.php';  ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Ingreso de Pedido Interno
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Inicio Seccion del area de la tabla -->
        <section class="col-lg-6 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                    <FORM ACTION="#" METHOD="post" id="myform">
                        <div class="form-group">
                            <br><label for="Puesto">Codigo Venta</label>
                            <input type="text" name="idPedidoInterno"  id="idPedidoInterno" class="form-control" readonly>
                            <label for="Puesto">MATERIA PRIMA</label>
                            <?php getInputSelect('ID_MATERIA_PRIMA','NOMBRE_MATERIA_PRIMA','MATERIAS_PRIMAS',$con); ?>
                            <br><label for="Puesto">Cantidad de Unidades Solicitadas</label>
                            <input type="number" name="CantidadMateria"  id="CantidadMateria" class="form-control">
                            <br><label for="Puesto">Tiempo Solicitud (minutos)</label>
                            <input type="number" name="TiempoSolicitud"  id="TiempoSolicitud" class="form-control">
                            <label for="Puesto"><br>SOLICITANTE</label>
                            <?php getInputSelect('EMPLEADO','PRIMER_NOMBRE','EMPLEADO',$con); ?>
                        </div>
                        <div class="form-group">
                            <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger">
                            <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger" onclick="guardandoOK()">
                        </div>
                    </FORM>
                </div>
            </div>
        </section>
        <section class="col-lg-7 connectedSortable" style="width: 950px">
            <div class="box box-solid bg-light-blue-gradient ">
                <div class="form-group">
                    <table class="table table-bordered">
                        <caption>
                        </caption>
                        <tr>
                            <td>CODIGO</td>
                            <td>MATERIA PRIMA</td>
                            <td>CANTIDAD MATERIA PRIMA</td>
                            <td>TIEMPO SOLICITUD</td>
                            <td>SOLICITANTE</td>
                            <td>TIEMPO_MAXIMO</td>
                            <td>TIEMPO_MINIMO</td>
                            <td>VALORACION</td>
                            <td>FECHA REGISTRO</td>
                            <td>Editar</td>
                            <td>Eliminar</td>
                        </tr>
                        <?php
                        $sel= $con->query("SELECT
    PIN.ID_PEDIDO_INT,
    MP.NOMBRE_MATERIA_PRIMA,
    PIN.CANTIDAD,
    PIN.TIEMPO_SOLICITUD,
    E.PRIMER_NOMBRE,
    PIN.TIEMPO_MAXIMO,
    PIN.TIEMPO_MINIMO,
    PIN.VALORACION,
    PIN.FECHA_REGISTRO
FROM
    pedido_interno PIN
INNER JOIN
	materias_primas MP
ON
	PIN.ID_MATERIA_PRIMA = MP.ID_MATERIA_PRIMA
INNER JOIN
	empleado E 
ON	
	PIN.ID_EMPLEADO = E.EMPLEADO
ORDER BY
	PIN.FECHA_REGISTRO
DESC;");
                        while ($fila = $sel -> fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $fila['ID_PEDIDO_INT'] ?></td>
                                <td><?php echo $fila['NOMBRE_MATERIA_PRIMA'] ?></td>
                                <td><?php echo $fila['CANTIDAD'] ?></td>
                                <td><?php echo $fila['TIEMPO_SOLICITUD'] ?></td>
                                <td><?php echo $fila['PRIMER_NOMBRE'] ?></td>
                                <td><?php echo $fila['TIEMPO_MAXIMO'] ?></td>
                                <td><?php echo $fila['TIEMPO_MINIMO'] ?></td>
                                <td><?php echo $fila['VALORACION'] ?></td>
                                <td><?php echo $fila['FECHA_REGISTRO'] ?></td>
                                <td>
                                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion"
                                            onclick="editform('<?php echo $fila['ID_PEDIDO_INT'] ?>'
                                                ,'<?php echo $fila['ID_MATERIA_PRIMA'] ?>'
                                                ,'<?php echo $fila['CANTIDAD'] ?>'
                                                ,'<?php echo $fila['TIEMPO_SOLICITUD'] ?>'
                                                ,'<?php echo $fila['PRIMER_NOMBRE'] ?>')">
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-danger glyphicon glyphicon-remove"
                                            onclick="preguntarSiNo('<?php echo $fila['ID_PEDIDO_INT'] ?>'
                                                ,'<?php echo $fila['ID_MATERIA_PRIMA'] ?>'
                                                ,'<?php echo $fila['CANTIDAD'] ?>'
                                                ,'<?php echo $fila['TIEMPO_SOLICITUD'] ?>'
                                                ,'<?php echo $fila['PRIMER_NOMBRE'] ?>')">
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </section>
    </div>
</section>
<script>
    $(function () {
        $('#myform').prop('action', './pages/forms/InsertarPedidoInterno.php');
        $('.textarea').wysihtml5()
    })
    function editform(id, nmp, cm, ts, ne){
        $("#idPedidoInterno").val(id);
        $("#ID_MATERIA_PRIMA").val(nmp);
        $("#CantidadMateria").val(cm);
        $("#TiempoSolicitud").val(ts);
        $("#ID_EMPLEADO").val(ne);
        $('#myform').prop('action', './pages/forms/ModificarPedidoInterno.php');
    }
    function preguntarSiNo(id, nmp, cm, ts, ne){
        $("#idPedidoInterno").val(id);
        $("#ID_MATERIA_PRIMA").val(nmp);
        $("#CantidadMateria").val(cm);
        $("#TiempoSolicitud").val(ts);
        $("#ID_EMPLEADO").val(ne);
        $('#myform').prop('action', './pages/forms/EliminarPedidoInterno.php');
    }
    function guardandoOK() {
        $('#myform').prop('action', './pages/forms/InsertarPedidoInterno.php');
        $('.textarea').wysihtml5()
    }
</script>