<?php include 'conexion.php'; include 'funciones.php';  ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Elección de Proveedores
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Inicio Seccion del area de la tabla -->
        <section class="col-lg-6 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                    <FORM ACTION="#" METHOD="post" id="myform">
                        <div class="form-group">
                            <br><label for="Puesto">Codigo</label>
                            <input type="text" name="idEleccion"  id="idEleccion" class="form-control" readonly>
                            <label for="Puesto">PROVEEDOR</label>
                            <?php getInputSelect('ID_PROVEEDOR','PROVEEDOR','PROVEEDORES',$con); ?>
                            <label for="Puesto"><br>EMPLEADO</label>
                            <?php getInputSelect('EMPLEADO','PRIMER_NOMBRE','EMPLEADO',$con); ?>
                            <br><label for="Puesto">Fecha de Solicitud</label>
                            <input type="date" name="FechaSolicitud"  id="FechaSolicitud" class="form-control">
                            <br><label for="Puesto">Fecha de Contestación</label>
                            <input type="date" name="FechaContestacion"  id="FechaContestacion" class="form-control">
                        </div>
                        <div class="form-group">
                            <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger">
                            <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger" onclick="guardandoOK()">
                        </div>
                    </FORM>
                </div>
            </div>
        </section>
        <section class="col-lg-7 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="form-group">
                    <table class="table table-bordered">
                        <caption>
                        </caption>
                        <tr>
                            <td>CODIGO</td>
                            <td>PROVEEDOR</td>
                            <td>EMPLEADO</td>
                            <td>FECHA DE SOLICITUD</td>
                            <td>FECHA DE CONTESTACIÓN</td>
                            <td>FECHA REGISTRO</td>
                            <td>Editar</td>
                            <td>Eliminar</td>
                        </tr>
                        <?php
                        $sel= $con->query("SELECT
    CV.ID_ELECCION_PROV,
    P.PROVEEDOR,
    E.PRIMER_NOMBRE,
    CV.FECHA_SOLICITUD,
    CV.FECHA_CONTESTACION,
    CV.FECHA_REGISTRO
FROM
    ELECCION_PROVEEDORES CV
INNER JOIN
	PROVEEDORES P 
ON
	CV.ID_PROVEEDOR = P.ID_PROVEEDOR
INNER JOIN
	empleado E 
ON	
	CV.ID_EMPLEADO = E.EMPLEADO
ORDER BY
	CV.FECHA_REGISTRO
DESC;");
                        while ($fila = $sel -> fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $fila['ID_ELECCION_PROV'] ?></td>
                                <td><?php echo $fila['PROVEEDOR'] ?></td>
                                <td><?php echo $fila['PRIMER_NOMBRE'] ?></td>
                                <td><?php echo $fila['FECHA_SOLICITUD'] ?></td>
                                <td><?php echo $fila['FECHA_CONTESTACION'] ?></td>
                                <td><?php echo $fila['FECHA_REGISTRO'] ?></td>
                                <td>
                                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion"
                                            onclick="editform('<?php echo $fila['ID_ELECCION_PROV'] ?>'
                                                ,'<?php echo $fila['PROVEEDOR'] ?>'
                                                ,'<?php echo $fila['PRIMER_NOMBRE'] ?>'
                                                ,'<?php echo $fila['FECHA_SOLICITUD'] ?>'
                                                ,'<?php echo $fila['FECHA_CONTESTACION'] ?>')">
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-danger glyphicon glyphicon-remove"
                                            onclick="preguntarSiNo('<?php echo $fila['ID_ELECCION_PROV'] ?>'
                                                    ,'<?php echo $fila['PROVEEDOR'] ?>'
                                                    ,'<?php echo $fila['PRIMER_NOMBRE'] ?>'
                                                    ,'<?php echo $fila['FECHA_SOLICITUD'] ?>'
                                                    ,'<?php echo $fila['FECHA_CONTESTACION'] ?>')">
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </section>
    </div>
</section>
<script>
    $(function () {
        $('#myform').prop('action', './pages/forms/InsertarEleccionP.php');
        $('.textarea').wysihtml5()
    })
    function editform(id, pr, ne, fs, fc){
        $("#idEleccion").val(id);
        $("#ID_PROVEEDOR").val(pr);
        $("#ID_EMPLEADO").val(ne);
        $("#FechaSolicitud").val(fs);
        $("#FechaContestacion").val(fc);
        $('#myform').prop('action', './pages/forms/ModificarEleccionP.php');
    }
    function preguntarSiNo(id, pr, ne, fs, fc){
        $("#idEleccion").val(id);
        $("#ID_PROVEEDOR").val(pr);
        $("#ID_EMPLEADO").val(ne);
        $("#FechaSolicitud").val(fs);
        $("#FechaContestacion").val(fc);
        $('#myform').prop('action', './pages/forms/EliminarEleccionP.php');
    }
    function guardandoOK() {
        $('#myform').prop('action', './pages/forms/InsertarËleccionP.php');
        $('.textarea').wysihtml5()
    }
</script>