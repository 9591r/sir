<?php include 'conexion.php'; include 'funciones.php'; ?>
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Empleados
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">

        <!-- Inicio Seccion del area de la tabla -->

        <section class="col-lg-4 connectedSortable">
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              
              <FORM id="myform" METHOD="post"> 
                
                <div class="form-group">
                <label for="PriNombreEmp">Primer Nombre</label>
                <input type="text" id="idNombre" name="idNombre" style="display: none;" class="form-control"> 
                <input type="text" name="PriNombreEmp" class="form-control">            
                

                <div class="form-group">
                <label for="SegNombrEmp">Segundo Nombre</label>
                <input type="text" name="SegNombreEmp" class="form-control">            
                


                <label for="PriApellidoEmp">Primer Apellido</label>
                <input type="text" name="PriApellidoEmp" class="form-control">            
                

                
                <label for="SegApellidoEmp">Segundo Apellido</label>
                <input type="text" name="SegApellidoEmp" class="form-control">            
                

                
                <label for="DPIEmp">DPI Empleado</label>
                <input type="text" name="DPIEmp" class="form-control" value="">            
                

                
                <label for="DireccionEmp">Direccion Empleado</label>
                <input type="text" name="DireccionEmp" class="form-control">            
                

              
                <label for="TelefonoEmp">Telefono Empleado</label>
                <input type="text" name="TelefonoEmp" class="form-control">            
                

                
                <label for="IDPuesto">ID Puesto</label>
                <?php getInputSelect('PUESTO','NOMBRE_PUESTO', 'PUESTO',$con); ?>                         
                </div>
                
                <div class="form-group">
                <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger"> 
                <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger">
                </div>

              </FORM> 

              </div>
            </div>
          </section>

          <!-- Tabla de resultados -->

            <section class="col-lg-8 connectedSortable">
              <div class="box box-solid bg-light-blue-gradient">

                  <div class="form-group">
                    <table class="table table-bordered">

                    <caption>
                  </caption>

                    <tr>
                    <td>ID Empleado</td>
                    <td>Pri. Nombre</td>
                    <td>Seg. Nombre</td>
                    <td>Pri. Apellido</td>
                    <td>Seg. Apellido</td>
                    <td>DPI</td>
                    <td>Direccion</td>
                    <td>Telefono</td>
                    <td>Puesto</td>
                    <td>Editar</td>
                    <td>Eliminar</td>
                  </tr>

                   <?php
        
                    $sel= $con->query("SELECT EMPLEADO.*, PUESTO.NOMBRE_PUESTO FROM EMPLEADO INNER JOIN PUESTO ON EMPLEADO.PUESTO = PUESTO.PUESTO");
                    while ($fila = $sel -> fetch_assoc()) {
                  ?>

                   <tr>
                    <td><?php echo $fila['EMPLEADO'] ?></td>
                    <td><?php echo $fila['PRIMER_NOMBRE'] ?></td>
                    <td><?php echo $fila['SEGUNDO_NOMBRE'] ?></td>
                    <td><?php echo $fila['PRIMER_APELLIDO'] ?></td>
                    <td><?php echo $fila['SEGUNDO_APELLIDO'] ?></td>
                    <td><?php echo $fila['DPI'] ?></td>
                    <td><?php echo $fila['DIRECCION'] ?></td>
                    <td><?php echo $fila['TELEFONO'] ?></td>
                    <td><?php echo $fila['NOMBRE_PUESTO'] ?></td>
                    <td>
                    <?php
                      $valor = "'".$fila['EMPLEADO']."','".$fila['PRIMER_NOMBRE']."','".$fila['SEGUNDO_NOMBRE']."','".$fila['PRIMER_APELLIDO']."','".$fila['SEGUNDO_APELLIDO']."','".$fila['DPI']."','".$fila['DIRECCION']."','".$fila['TELEFONO']."','".$fila['PUESTO']."'";
                    ?>
                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion" onclick="editform(<?php echo $valor ?>)">
                    </button>
                    </td>
                    <td>
                    <button class="btn btn-danger glyphicon glyphicon-remove" 
                    onclick="preguntarSiNo('<?php echo $fila['EMPLEADO'] ?>')">
                    </button>
                    </td>
                  </tr>

                   <?php } ?>

                   </table>

              </div>
            </div>

          <!-- Fin de tabla de resultados -->


                    
        <!-- Fin de la seccion de los botones -->

        </div> 
        <!-- Fin div del area de contenido -->

    </section>
    <script>
  $(function () {
     $('#myform').prop('action', './pages/forms/InsertarEmpleado.php');
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  function editform(id, nombre1, nombre2,nombre3,nombre4,nombre5,nombre6,nombre7,nombre8){
     $("#idNombre").val(id);
             
     $('[name="PriNombreEmp"').val(nombre1);
     $('[name="SegNombreEmp"').val(nombre2);
     $('[name="PriApellidoEmp"').val(nombre3);
     $('[name="SegApellidoEmp"').val(nombre4);
     $('[name="DPIEmp"').val(nombre5);
     $('[name="DireccionEmp"').val(nombre6);
     $('[name="TelefonoEmp"').val(nombre7);
     $('[name="PUESTO"').val(nombre8);

      $("#myform").prop('action', './pages/forms/ModificarEmpleado.php');
    //alert("modifica");
  }
  function preguntarSiNo(id, nombre1, nombre2,nombre3,nombre4,nombre5,nombre6,nombre7,nombre8){
    $("#idNombre").val(id);

     $('[name="PriNombreEmp"').val(nombre1);
     $('[name="SegNombreEmp"').val(nombre2);
     $('[name="PriApellidoEmp"').val(nombre3);
     $('[name="SegApellidoEmp"').val(nombre4);
     $('[name="DPIEmp"').val(nombre5);
     $('[name="DireccionEmp"').val(nombre6);
     $('[name="TelefonoEmp"').val(nombre7);
     $('[name="PUESTO"').val(nombre8);

     $('#myform').prop('action', './pages/forms/EliminarEmpleado.php');
    //alert("eliminar");
  }
</script>