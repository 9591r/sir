<?php include 'conexion.php'; include 'funciones.php';  ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Tiempo de Espera de Pedidos
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Inicio Seccion del area de la tabla -->
        <section class="col-lg-6 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                    <FORM ACTION="#" METHOD="post" id="myform">
                        <div class="form-group">
                            <br><label for="Puesto">Codigo Pedido</label>
                            <input type="text" name="idPedido"  id="idPedido" class="form-control" readonly>
                            <label for="Puesto">PROVEEDOR</label>
                            <?php getInputSelect('ID_PROVEEDOR','PROVEEDOR','PROVEEDORES',$con); ?>
                            <label for="Puesto"><br>SOLICITANTE</label>
                            <?php getInputSelect('EMPLEADO','PRIMER_NOMBRE','EMPLEADO',$con); ?>
                            <br><label for="Puesto">Tiempo Solicitud (minutos)</label>
                            <input type="number" name="TiempoSolicitud"  id="TiempoSolicitud" class="form-control">

                        </div>
                        <div class="form-group">
                            <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger">
                            <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger" onclick="guardandoOK()">
                        </div>
                    </FORM>
                </div>
            </div>
        </section>
        <section class="col-lg-7 connectedSortable" style="width: 950px">
            <div class="box box-solid bg-light-blue-gradient ">
                <div class="form-group">
                    <table class="table table-bordered">
                        <caption>
                        </caption>
                        <tr>
                            <td>CODIGO</td>
                            <td>PROVEEDOR</td>
                            <td>SOLICITANTE</td>
                            <td>TIEMPO SOLICITUD</td>
                            <td>TIEMPO_MAXIMO</td>
                            <td>TIEMPO_MINIMO</td>
                            <td>FECHA REGISTRO</td>
                            <td>Editar</td>
                            <td>Eliminar</td>
                        </tr>
                        <?php
                        $sel= $con->query("SELECT
    PIN.ID_TIEMPO_ESPERA_PEDIDO,
    MP.PROVEEDOR,
    E.PRIMER_NOMBRE,
    PIN.TIEMPO_REAL,
    PIN.TIEMPO_MAXIMO,
    PIN.TIEMPO_MINIMO,
    PIN.FECHA_REGISTRO
FROM
    tiempo_espera_pedido PIN
INNER JOIN
	PROVEEDORES MP
ON
	PIN.ID_PROVEEDOR = MP.ID_PROVEEDOR
INNER JOIN
	empleado E 
ON	
	PIN.ID_EMPLEADO = E.EMPLEADO
ORDER BY
	PIN.FECHA_REGISTRO
DESC;");
                        while ($fila = $sel -> fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $fila['ID_TIEMPO_ESPERA_PEDIDO'] ?></td>
                                <td><?php echo $fila['PROVEEDOR'] ?></td>
                                <td><?php echo $fila['PRIMER_NOMBRE'] ?></td>
                                <td><?php echo $fila['TIEMPO_REAL'] ?></td>
                                <td><?php echo $fila['TIEMPO_MAXIMO'] ?></td>
                                <td><?php echo $fila['TIEMPO_MINIMO'] ?></td>
                                <td><?php echo $fila['FECHA_REGISTRO'] ?></td>
                                <td>
                                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion"
                                            onclick="editform('<?php echo $fila['ID_TIEMPO_ESPERA_PEDIDO'] ?>'
                                                    ,'<?php echo $fila['PROVEEDOR'] ?>'
                                                    ,'<?php echo $fila['PRIMER_NOMBRE'] ?>'
                                                    ,'<?php echo $fila['TIEMPO_REAL'] ?>')">
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-danger glyphicon glyphicon-remove"
                                            onclick="preguntarSiNo('<?php echo $fila['ID_TIEMPO_ESPERA_PEDIDO'] ?>'
                                                    ,'<?php echo $fila['PROVEEDOR'] ?>'
                                                    ,'<?php echo $fila['PRIMER_NOMBRE'] ?>'
                                                    ,'<?php echo $fila['TIEMPO_REAL'] ?>')">
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </section>
    </div>
</section>
<script>
    $(function () {
        $('#myform').prop('action', './pages/forms/InsertarPedidoE.php');
        $('.textarea').wysihtml5()
    })
    function editform(id, p, ne, tr){
        $("#idPedido").val(id);
        $("#ID_PROVEEDOR").val(p);
        $("#ID_EMPLEADO").val(ne);
        $("#TiempoSolicitud").val(tr);
        $('#myform').prop('action', './pages/forms/ModificarPedidoE.php');
    }
    function preguntarSiNo(id, p, ne, tr){
        $("#idPedido").val(id);
        $("#ID_PROVEEDOR").val(p);
        $("#ID_EMPLEADO").val(ne);
        $("#TiempoSolicitud").val(tr);
        $('#myform').prop('action', './pages/forms/EliminarPedidoE.php');
    }
    function guardandoOK() {
        $('#myform').prop('action', './pages/forms/InsertarPedidoE.php');
        $('.textarea').wysihtml5()
    }
</script>