<?php


function getInputSelect($ide,$campo, $tabla,$con){
    $input = "<select name='$ide' class='form-control' style='color: black;'required>";
    $sel= $con->query("SELECT $ide,$campo FROM $tabla");
        while ($fila = $sel -> fetch_assoc()) {
            $input .= "<option value='".$fila[$ide]."'>".$fila[$campo]."</option>";
        }
    $input .= '</select>';
    echo $input;
}

function getInputSelectWhere($ide,$campo, $sql ,$con){
    $input = "<select name='$ide' class='form-control' style='color: black;'required>";
    $sel= $con->query($sql);
        while ($fila = $sel -> fetch_assoc()) {
            $input .= "<option value='".$fila[$ide]."'>".$fila[$campo]."</option>";
        }
    $input .= '</select>';
    echo $input;
}

function setJSBoxMessage($mensaje){
    echo "<script>
        alert('$mensaje');
    </script>";
}

/*
*ARCHIVO COMENTADO 27/01/2018
*FUNCIONES QUE LLENAN LA INFORMACION DE PORTAL INDICADORES
*/
function hacerGrafica($nombreGrafica,$nombreId,$valColumnas,$paisNombre,$porcentajePais,$paisColor,$meta){

    $contadorPais = count($paisNombre);
    $contadorColumnas= count($valColumnas)
        //---------------------------------------------------------------------
        ?>
        <div  style='width:100%; height:300px;' ><canvas id='<?php echo "grafica".$nombreId; ?>' <!--data-toggle="modal" data-target="#<?php //echo $tecnologia.$dilacionNo.$indicador.$yyyy;?>"--> ></canvas></div>
        <!--<a id="download<?php echo $nombreId; ?>">DESCARGAR</a>-->
       <script>
    
      // document.getElementById("download<?php echo $nombreId; ?>").addEventListener('click', function() {
        // downloadCanvas(this, '<?php echo "grafica".$nombreId; ?>', '<?php echo "grafica".$nombreId; ?>.jpg'); }, false);
    
        window.chartColors = {
                white: 'rgb(255, 255, 255)',
                red: 'rgb(255, 0, 0)',
                orange: 'rgb(255, 159, 64)',
                yellow: 'rgb(255, 205, 86)',
                green: 'rgb(77, 255, 77)',
                blue: 'rgb(54, 162, 235)',
                purple: 'rgb(153, 102, 255)',
                grey: 'rgb(150, 150, 150)',
                meta: 'rgb(0, 0, 0)',
                meta1: 'rgb(256, 128, 128)'
        };
            var <?php echo "configGrafica".$nombreId ?> = {
                type: 'line',
                data: {
                    labels: [
    
            <?php for($dia=0; $dia<$contadorColumnas;$dia++){
                if($dia!=0){
                   echo ",";
                }
                   echo "'".($valColumnas[$dia])."'";    
            } ?>
    
                ],
                    datasets: [
    
                    {
                        label: "META",
                        fill: false,
                        backgroundColor: window.chartColors.meta,
                        borderColor: window.chartColors.meta,
                        data: [
                          <?php
                            for($x=0;$x<$contadorColumnas;$x++){
                              if($x!=0){
                                    echo ",";
                              }
                              echo ($meta);
                            }
                          ?>
                        ],
                         pointRadius: 1,
                         pointHoverRadius: 1,
                         pointHitRadius: 1,
                         pointBorderWidth: 1,
                         borderWidth: 5,
                         borderDash: [10, 5],
               fill: false,
                    }
    
                    <?php
                    //FOR QUE TIENE LA INFORMACION DE LOS PORCENTAJES
                    for($p=0; $p<$contadorPais; $p++){
                    if($porcentajePais[$p][0] != null){?>
                    ,{
                        //nombre del pais
                        label: "<?php echo $paisNombre[$p]; ?>",
                        fill: false,
                        //color del pais
                        backgroundColor: '<?php echo $paisColor[$p]; ?>',
                        borderColor: '<?php echo $paisColor[$p]; ?>',
                        data: [
                        <?php
    
                        //SI ES DIFERENTE A NULO NO PRESENTA EL PAIS EN LA GRAFICA
                        //if($porcentajePais[$p][0]!= null){
                            for($x=0;$x<$contadorColumnas;$x++){
                              if($x!=0){
                                   echo ",";
                                }
                                if(!isset($porcentajePais[$p][$x])||$porcentajePais[$p][$x]==0 || $porcentajePais[$p][$x]== null || is_null($porcentajePais[$p][$x] || $porcentajePais[$p][$x]== 'NULL')){
                                    echo "null";
                                }else{
                                    echo $porcentajePais[$p][$x];
                                }
                            }
                        //}
    
                          ?>
                        ],
                        lineTension: 0,
                        fill: false,
                    }<?php }} // fin de for pais ?>
                  ]
                },
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    title:{
                        display:true,
    
                        text:'<?php echo $nombreGrafica; ?>'
                                           ////NOMBRE DE LA tecnologia A USAR
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                         callbacks: {
                           /* label: function(t, d) {
                                  var xLabel = d.datasets[t.datasetIndex].label;
                                  if (isNaN(t.yLabel) === false){
                                    var yLabel = t.yLabel + '%'
                                    return xLabel + ': ' + yLabel;
                                  }else{
                                    return '';
    
                                  }
                            }*/
                         },
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'MES'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'CANTIDAD'
                            },
                            ticks: {
                                /*suggestedMin: 80,*/
                                // Max: 100,
                                // callback: function(value){return (Math.round(value * 100) / 100 )+ "%"}
                            },
    
                        }]
                    },elements: {
                        arc: {
                    },
                        point: {},
                        line: {tension:0.01,fill:false,borderWidth:2//,borderDash:[12,4],
                    },
                        rectangle: {
                    },
                    },
                    animation:{
                        easing:'easeInQuad',
                        duration: 300
                        }
                }
            };
    
                    var <?php  echo "ctx".$nombreId ?> = document.getElementById("<?php  echo "grafica".$nombreId; ?>").getContext("2d");
                        window.myLine = new Chart(<?php  echo "ctx".$nombreId.", configGrafica".$nombreId; ?>);
                    var colorNames = Object.keys(window.chartColors);
                    //console.log(<?php echo "configGrafica".$nombreId ?>);
       </script>
    
        <?php
        //---------------------------------------------------------
    }
//==============================================================================================================================================
//TABLA GRAFICA BARRAS

function hacerGraficaBarras($nombreGrafica,$nombreId,$valColumnas,$paisNombre,$porcentajePais,$paisColor,$meta){

    $contadorPais = count($paisNombre);
    $contadorColumnas= count($valColumnas)
    //---------------------------------------------------------------------
    ?>
    <div  style='width:100%; height:300px;' ><canvas id='<?php echo "grafica".$nombreId; ?>' <!--data-toggle="modal" data-target="#<?php //echo $tecnologia.$dilacionNo.$indicador.$yyyy;?>"--> ></canvas></div>
    <!--<a id="download<?php echo $nombreId; ?>">DESCARGAR</a>-->
    <script>

        // document.getElementById("download<?php echo $nombreId; ?>").addEventListener('click', function() {
        // downloadCanvas(this, '<?php echo "grafica".$nombreId; ?>', '<?php echo "grafica".$nombreId; ?>.jpg'); }, false);

        window.chartColors = {
            white: 'rgb(255, 255, 255)',
            red: 'rgb(255, 0, 0)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(77, 255, 77)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(150, 150, 150)',
            meta: 'rgb(0, 0, 0)',
            meta1: 'rgb(256, 128, 128)'
        };
        var <?php echo "configGrafica".$nombreId ?> = {
            type: 'bar',
                data: {
                labels: [

                    <?php for($dia=0; $dia<$contadorColumnas;$dia++){
                    if($dia!=0){
                        echo ",";
                    }
                    echo "'".($valColumnas[$dia])."'";
                } ?>

                ],
                    datasets: [

                    {
                        label: "META",
                        fill: false,
                        backgroundColor: window.chartColors.meta,
                        borderColor: window.chartColors.meta,
                        data: [
                            <?php
                            for($x=0;$x<$contadorColumnas;$x++){
                                if($x!=0){
                                    echo ",";
                                }
                                echo ($meta);
                            }
                            ?>
                        ],
                        pointRadius: 1,
                        pointHoverRadius: 1,
                        pointHitRadius: 1,
                        pointBorderWidth: 1,
                        borderWidth: 5,
                        borderDash: [10, 5],
                        fill: false,
                    }

                    <?php
                    //FOR QUE TIENE LA INFORMACION DE LOS PORCENTAJES
                    for($p=0; $p<$contadorPais; $p++){
                    if($porcentajePais[$p][0] != null){?>
                    ,{
                        //nombre del pais
                        label: "<?php echo $paisNombre[$p]; ?>",
                        fill: false,
                        //color del pais
                        backgroundColor: '<?php echo $paisColor[$p]; ?>',
                        borderColor: '<?php echo $paisColor[$p]; ?>',
                        data: [
                            <?php

                            //SI ES DIFERENTE A NULO NO PRESENTA EL PAIS EN LA GRAFICA
                            //if($porcentajePais[$p][0]!= null){
                            for($x=0;$x<$contadorColumnas;$x++){
                                if($x!=0){
                                    echo ",";
                                }
                                if(!isset($porcentajePais[$p][$x])||$porcentajePais[$p][$x]==0 || $porcentajePais[$p][$x]== null || is_null($porcentajePais[$p][$x] || $porcentajePais[$p][$x]== 'NULL')){
                                    echo "null";
                                }else{
                                    echo $porcentajePais[$p][$x];
                                }
                            }
                            //}

                            ?>
                        ],
                        lineTension: 0,
                        fill: false,
                    }<?php }} // fin de for pais ?>
                ]
            },
            options: {
                maintainAspectRatio: false,
                    responsive: true,
                    title:{
                    display:true,

                        text:'<?php echo $nombreGrafica; ?>'
                    ////NOMBRE DE LA tecnologia A USAR
                },
                tooltips: {
                    mode: 'index',
                        intersect: false,
                        callbacks: {
                        /* label: function(t, d) {
                               var xLabel = d.datasets[t.datasetIndex].label;
                               if (isNaN(t.yLabel) === false){
                                 var yLabel = t.yLabel + '%'
                                 return xLabel + ': ' + yLabel;
                               }else{
                                 return '';

                               }
                         }*/
                    },
                },
                hover: {
                    mode: 'nearest',
                        intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'MES'
                        }
                    }],
                        yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'PORCENTAJE'
                        },
                        ticks: {
                            /*suggestedMin: 80,*/
                            // Max: 100,
                            // callback: function(value){return (Math.round(value * 100) / 100 )+ "%"}
                        },

                    }]
                },elements: {
                    arc: {
                    },
                    point: {},
                    line: {tension:0.01,fill:false,borderWidth:2
                        //,borderDash:[12,4],
                    },
                    rectangle: {
                    },
                },
                animation:{
                    easing:'linear',
                }
            }
        };

        var <?php  echo "ctx".$nombreId ?> = document.getElementById("<?php  echo "grafica".$nombreId; ?>").getContext("2d");
        window.myLine = new Chart(<?php  echo "ctx".$nombreId.", configGrafica".$nombreId; ?>);
        var colorNames = Object.keys(window.chartColors);
        //console.log(<?php echo "configGrafica".$nombreId ?>);
    </script>

    <?php
    //---------------------------------------------------------
}






//=============================================================================================================================================
    
    // TABLA DEBAJO DE GRAFICAS
function Desplegar_Tabla($nombre,$nombreColumna,$nombreTitulos,$nombreValores,$nombreColores,$valores,$meta){

    $pais = $nombreValores;
    $contadorPais = count($pais);
    $paisNombre = $nombreValores;
    $paisTd = $nombreColores;
    //$idBoton = "'$nombre','undefined'";
    $idBoton = '"'.$nombre.'","Span'.$nombre.'"';
    //BOTON DONDE DESPLIEGA LA INFORMACION
    $columnas = count($nombreTitulos);
    
    $tabla = '
    <h6 style="padding-top: 10%;">
    <div name="tabla" class="table-responsive" style=";">
    <table class="table" style=" /*border: 1px; padding: 0.1px; font-size: 10px*/">
                <thead>';
            // AGREGA LOS CAMPOS DE TITULOS A LA TABLA
            for($x=0; $x<$columnas; $x++){
                if($x == 0){
                    $tabla .= "<th style='width:2%'></th><th class='table-success' style='position:stiky;'>$nombreColumna</th>";
                }
                $tabla .= "<th class='text-center' style='align: center;' align='center'>".($nombreTitulos[$x])."</th>";
    
            }
    $tabla .= "</thead>";
    
    $tabla .= "<tbody>";
            // AGREGA LOS CAMPOS DE PORCENTAJES  A LA TABLA
            for($y=0; $y<$contadorPais; $y++){
                if($valores[$y][0]!=""){
                $tabla .= "<tr>";
                for($x=0; $x<$columnas; $x++){
                    // COMIENZA CON -1 PARA AGREGAR EL NOMBRE DEL PAIS
                    if($x == 0){
                        $tabla .= "<td style='width:2%; background-color: ".$paisTd[$y]."'></td><td>".$pais[$y]."</td>";
                    }
                        // LOS PORCENTAJES DE LAS TABLAS
                        //SI NO HAY INFORMACION LE AGREGA UNA CELDA VACIA
                        if( empty($valores[$y][$x]) || ($valores[$y][$x]) == "0"){
                            $tabla .="<td></td>";
                        }
                        else{
                            //META A LA MENOR
                            if($valores[$y][$x] == "NULL"){
    
                                $tabla .= "<td></td>";
                            //CERCA A LA META
                            }
                            //META A LA MENOR
                            /*elseif($valores[$y][$x] < (($meta-0.036)*100)){
    
                                $tabla .= "<td class='table table-default' style='background-color: #f42e2e; color: #ffffff;'>".$valores[$y][$x]."%</td>";
                            //CERCA A LA META
                            }elseif($valores[$y][$x] < (($meta*100))){
    
                                $tabla .= "<td class='table table-default' style='background-color: #e6e600;'><font color='#000000' >".$valores[$y][$x]."%</td>";
                            //MAYOR A LA META
                            }*/
                            else{
    
                                $tabla .= "<td style='align: center;' align='center' >".$valores[$y][$x]."</td>";
                            }
                        }
    
                }
                $tabla .= "</tr>";
                }
            }
    //LLENADO DE TABLA TAGS DE CIERRE
    $tabla .= "</tbody>
            </table>
        </div>
    </h6>
            ";
        echo $tabla;
    }



?>