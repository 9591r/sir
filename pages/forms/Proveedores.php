<?php include 'conexion.php'; ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proveedores
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">

        <!-- Inicio Seccion del area de la tabla -->

        <section class="col-lg-6 connectedSortable">
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              
              <FORM ACTION="#" METHOD="post" id="myform"> 
               
                <div class="form-group">
                <label for="Proveedor">Proveedor</label>
                <input type="text" name="idProveedor"  id="idProveedor" class="form-control"  style="visibility:hidden"> 
                <input type="text" name="Proveedor"  id="Proveedor" class="form-control">            
                        
                
                </div>
                
                <div class="form-group">
                <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger"> 
                <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger">
                </div>

              </FORM> 

              </div>
            </div>
          </section>

           <section class="col-lg-7 connectedSortable">
              <div class="box box-solid bg-light-blue-gradient">

                  <div class="form-group">
                    <table class="table table-bordered">

                    <caption>
                  </caption>

                    <tr>
                    <td>ID Proveedor</td>
                    <td>Proveedor</td>
                    <td>Editar</td>
                    <td>Eliminar</td>
                  </tr>

                   <?php
        
                    $sel= $con->query("SELECT * FROM PROVEEDORES");
                    while ($fila = $sel -> fetch_assoc()) {
                  ?>

                   <tr>
                    <td><?php echo $fila['PROVEEDOR'] ?></td>
                    <td><?php echo $fila['NOMBRE_PROVEEDOR'] ?></td>
                    <td>
                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion" onclick="editform('<?php echo $fila['PROVEEDOR'] ?>', '<?php echo $fila['NOMBRE_PROVEEDOR'] ?>')">
                    </button>
                    </td>
                    <td>
                    <button class="btn btn-danger glyphicon glyphicon-remove" 
                    onclick="preguntarSiNo('<?php echo $fila['PROVEEDOR'] ?>', '<?php echo $fila['NOMBRE_PROVEEDOR'] ?>')">
                    </button>
                    </td>
                  </tr>

                   <?php } ?>

                   </table>

              </div>
            </div>
          </section>

        </div> 
        <!-- Fin div del area de contenido -->

    </section>

<script>
  $(function () {
     $('#myform').prop('action', './pages/forms/InsertarProveedores.php');
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  function editform(id, nombre){
     $("#idProveedor").val(id);
     $("#Proveedor").val(nombre);
      $('#myform').prop('action', './pages/forms/ModificarProveedores.php');
    //alert("modifica");
  }
  function preguntarSiNo(id, nombre){
    $("#idProveedor").val(id);
     $("#Proveedor").val(nombre);
     $('#myform').prop('action', './pages/forms/EliminarProveedores.php');
    //alert("eliminar");
  }
</script>