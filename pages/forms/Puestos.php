<?php include 'conexion.php'; ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Puestos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Editors</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">

        <!-- Inicio Seccion del area de la tabla -->

        <section class="col-lg-6 connectedSortable">
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              
              <FORM ACTION="#" METHOD="post" id="myform"> 
               
                <div class="form-group">
                <label for="Puesto">Puesto</label>
                <input type="text" name="idPuesto"  id="idPuesto" class="form-control"  style="visibility:hidden"> 
                <input type="text" name="Puesto"  id="Puesto" class="form-control">            
                        
                
                </div>
                
                <div class="form-group">
                <INPUT TYPE="submit" VALUE="Guardar" class="btn btn-danger"> 
                <INPUT TYPE="reset" VALUE="Limpia" class="btn btn-danger">
                </div>

              </FORM> 

              </div>
            </div>
          </section>

           <section class="col-lg-7 connectedSortable">
              <div class="box box-solid bg-light-blue-gradient">

                  <div class="form-group">
                    <table class="table table-bordered">

                    <caption>
                  </caption>

                    <tr>
                    <td>ID Puesto</td>
                    <td>Puesto</td>
                    <td>Editar</td>
                    <td>Eliminar</td>
                  </tr>

                   <?php
        
                    $sel= $con->query("SELECT * FROM PUESTO");
                    while ($fila = $sel -> fetch_assoc()) {
                  ?>

                   <tr>
                    <td><?php echo $fila['PUESTO'] ?></td>
                    <td><?php echo $fila['NOMBRE_PUESTO'] ?></td>
                    <td>
                    <button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion" onclick="editform('<?php echo $fila['PUESTO'] ?>', '<?php echo $fila['NOMBRE_PUESTO'] ?>')">
                    </button>
                    </td>
                    <td>
                    <button class="btn btn-danger glyphicon glyphicon-remove" 
                    onclick="preguntarSiNo('<?php echo $fila['PUESTO'] ?>', '<?php echo $fila['NOMBRE_PUESTO'] ?>')">
                    </button>
                    </td>
                  </tr>

                   <?php } ?>

                   </table>

              </div>
            </div>
          </section>

          <!-- Seccion del area de la tabla -->


          <!-- Inicio de la seccion de los botones -->

          <!--

          <section class="col-lg-5 connectedSortable">
          <!-- Map box -->
          <!--
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              <form>
                <div>
                <label for="email">80% a 100% -- Excelente</label>
                </div>

                <div>
                <label for="email">50% a 79% -- Bueno</label>
                </div>

                <div>
                <label for="email">0% a 49% -- Malo</label>
                </div>
                <button type="button" class="btn btn-info btn-lg btn-block"><a href="N-SoporteLocal.html"> <--- REGRESAR </button>
                <button type="button" class="btn btn-danger btn-lg btn-block">KPI Empaque Adecuado del Inventario</button>
              </form> 
          </div>

        </section>
          -->
        <!-- Fin de la seccion de los botones -->

        </div> 
        <!-- Fin div del area de contenido -->

    </section>

<script>
  $(function () {
     $('#myform').prop('action', './pages/forms/InsertarPuestos.php');
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  function editform(id, nombre){
     $("#idPuesto").val(id);
     $("#Puesto").val(nombre);
      $('#myform').prop('action', './pages/forms/ModificarPuestos.php');
    //alert("modifica");
  }
  function preguntarSiNo(id, nombre){
    $("#idPuesto").val(id);
     $("#Puesto").val(nombre);
     $('#myform').prop('action', './pages/forms/EliminarPuestos.php');
    //alert("eliminar");
  }
</script>