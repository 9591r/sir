<html> 
<head> 
   <title>Ejemplo de PHP</title> 
</head> 
<body> 
<H1>Comparaci�n de n�meros</H1> 
<p>

<li><a href="KPI-SoporteLocal-TiempoDiag.php"><i class="fa fa-circle-o"></i> Inicio</a></li>
<?php 
/* LEER VARIABLES DE $_GET */
$n1=intval($_GET['numero1']);
/*$n2=intval($_GET['numero2']);*/
$n2=('80');/*EXCELENTE*/
$n3=('50');/*BUENO*/
$n4=('0');/*MALO*/

if ($n1>$n2){
    echo "El n�mero ingresado es (".$n1.") EXCELENTE";
}
elseif ($n1==$n2){
    echo "El n�mero ingresado es (".$n1.") EXCELENTE";
}
elseif ($n1==$n3){
    echo "El n�mero ingresado es (".$n1.") BUENO";
}
elseif ($n1>$n3 & $n1<$n2){
    echo "El n�mero ingresado es (".$n1.") BUENO";
}
elseif ($n1==$n4){
    echo "El n�mero ingresado es (".$n1.") MALO";
}
elseif ($n1>$n4 & $n1<$n3){
    echo "El n�mero ingresado es (".$n1.") MALO";
}

?>
</p>
<br> 

<script src="Chart.js"></script>
<div id="canvas-holder">
<canvas id="chart-area" width="300" height="300"></canvas>
<canvas id="chart-area2" width="300" height="300"></canvas>
<canvas id="chart-area3" width="600" height="300"></canvas>
<canvas id="chart-area4" width="600" height="300"></canvas>
</div>
<script>
var pieData = [{value: 40,color:"#0b82e7",highlight: "#0c62ab",label: "Google Chrome"},
				{
					value: 16,
					color: "#e3e860",
					highlight: "#a9ad47",
					label: "Android"
				},
				{
					value: 11,
					color: "#eb5d82",
					highlight: "#b74865",
					label: "Firefox"
				},
				{
					value: 10,
					color: "#5ae85a",
					highlight: "#42a642",
					label: "Internet Explorer"
				},
				{
					value: 8.6,
					color: "#e965db",
					highlight: "#a6429b",
					label: "Safari"
				}
			];

	var barChartData = {
		labels : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio"],
		datasets : [
			{
				fillColor : "#6b9dfa",
				strokeColor : "#ffffff",
				highlightFill: "#1864f2",
				highlightStroke: "#ffffff",
				data : [90,30,10,80,15,5,15]
			},
			{
				fillColor : "#e9e225",
				strokeColor : "#ffffff",
				highlightFill : "#ee7f49",
				highlightStroke : "#ffffff",
				data : [40,50,70,40,85,55,15]
			}
		]

	}	
		var lineChartData = {
			labels : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio"],
			datasets : [
				{
					label: "Primera serie de datos",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "#6b9dfa",
					pointColor : "#1e45d7",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [90,30,10,80,15,5,15]
				},
				{
					label: "Segunda serie de datos",
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "#e9e225",
					pointColor : "#faab12",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [40,50,70,40,85,55,15]
				}
			]

		}
var ctx = document.getElementById("chart-area").getContext("2d");
var ctx2 = document.getElementById("chart-area2").getContext("2d");
var ctx3 = document.getElementById("chart-area3").getContext("2d");
var ctx4 = document.getElementById("chart-area4").getContext("2d");
window.myPie = new Chart(ctx).Pie(pieData);	
window.myPie = new Chart(ctx2).Doughnut(pieData);				
window.myPie = new Chart(ctx3).Bar(barChartData, {responsive:true});
window.myPie = new Chart(ctx4).Line(lineChartData, {responsive:true});
</script>


</body> 
</html>